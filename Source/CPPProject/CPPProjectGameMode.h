// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameMode.h"
#include "GameHUD.h"
#include "CPPProjectGameMode.generated.h"

UCLASS(minimalapi)
class ACPPProjectGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	ACPPProjectGameMode();
};



