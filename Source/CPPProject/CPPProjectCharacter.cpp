// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "CPPProject.h"
#include "CPPProjectCharacter.h"

//////////////////////////////////////////////////////////////////////////
// ACPPProjectCharacter

ACPPProjectCharacter::ACPPProjectCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->AttachTo(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller


	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->AttachTo(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)
}

//////////////////////////////////////////////////////////////////////////
// Input

void ACPPProjectCharacter::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	// Set up gameplay key bindings
	check(InputComponent);
	InputComponent->BindAction("Jump", IE_Pressed, this, &ACPPProjectCharacter::CheckIfCastingBeforeJump);
	InputComponent->BindAction("Jump", IE_Released, this, &ACPPProjectCharacter::CheckIfCastingBeforeStopJumping);

	InputComponent->BindAxis("MoveForward", this, &ACPPProjectCharacter::MoveForward);
	InputComponent->BindAxis("MoveRight", this, &ACPPProjectCharacter::MoveRight);

	InputComponent->BindAction("ZoomIn", IE_Pressed, this, &ACPPProjectCharacter::CameraZoomIn);
	InputComponent->BindAction("ZoomOut", IE_Pressed, this, &ACPPProjectCharacter::CameraZoomOut);

	InputComponent->BindAction("RightMouseButtonPress", IE_Pressed, this, &ACPPProjectCharacter::RightMouseButtonPressed);
	InputComponent->BindAction("RightMouseButtonPress", IE_Released, this, &ACPPProjectCharacter::RightMouseButtonReleased);


	//MenuHandlers
	FInputActionBinding& toggleGameMenu = InputComponent->BindAction("InGameMenu", IE_Pressed, this, &ACPPProjectCharacter::ToggleMainMenu);
	toggleGameMenu.bExecuteWhenPaused = true;

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	InputComponent->BindAxis("Turn", this, &ACPPProjectCharacter::Turn);
	InputComponent->BindAxis("TurnRate", this, &ACPPProjectCharacter::TurnAtRate);
	InputComponent->BindAxis("LookUp", this, &ACPPProjectCharacter::LookUp);
	InputComponent->BindAxis("LookUpRate", this, &ACPPProjectCharacter::LookUpAtRate);
}


void ACPPProjectCharacter::TurnAtRate(float Rate)
{	
	// calculate delta for this frame from the rate information
	if (!RightButtonPressed) return;
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void ACPPProjectCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	if (!RightButtonPressed) return;
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void ACPPProjectCharacter::Turn(float Rate)
{
	if (!RightButtonPressed) return;
	AddControllerYawInput(Rate);
}

void ACPPProjectCharacter::LookUp(float Rate)
{
	if (!RightButtonPressed) return;
	AddControllerPitchInput(Rate);
}

void ACPPProjectCharacter::MoveForward(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		if (!Instance) Instance = Cast < UMainGameInstance>(GetGameInstance());

		if (Instance->SpellCastingManager->isCasting)
			Instance->SpellCastingManager->StopCasting();

		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void ACPPProjectCharacter::MoveRight(float Value)
{
	if ( (Controller != NULL) && (Value != 0.0f) )
	{
		if (!Instance) Instance = Cast < UMainGameInstance>(GetGameInstance());

		if (Instance->SpellCastingManager->isCasting)
			Instance->SpellCastingManager->StopCasting();

		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
	
		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}


void ACPPProjectCharacter::CheckIfCastingBeforeJump()
{
	if (!Instance) Instance = Cast < UMainGameInstance>(GetGameInstance());

	if (Instance->SpellCastingManager->isCasting)
		Instance->SpellCastingManager->StopCasting();

	Jump();
}

void ACPPProjectCharacter::CheckIfCastingBeforeStopJumping()
{
	if (!Instance) Instance = Cast < UMainGameInstance>(GetGameInstance());

	if (Instance->SpellCastingManager->isCasting)
		Instance->SpellCastingManager->StopCasting();

	StopJumping();
}

void ACPPProjectCharacter::CameraZoomIn()
{
	if (CameraBoom->TargetArmLength < 350) return;
	CameraBoom->TargetArmLength = CameraBoom->TargetArmLength - 100;
}

void ACPPProjectCharacter::CameraZoomOut()
{
	if (CameraBoom->TargetArmLength > 1200) return;
	CameraBoom->TargetArmLength = CameraBoom->TargetArmLength + 100;
}

void ACPPProjectCharacter::RightMouseButtonPressed()
{
	RightButtonPressed = true;
}

void ACPPProjectCharacter::RightMouseButtonReleased()
{
	RightButtonPressed = false;
}

float ACPPProjectCharacter::TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	/*if (Instance == NULL) Instance = Cast<UMyGameInstance>(GetGameInstance());
	if (DamageEvent.DamageTypeClass->IsChildOf(UHeal_DamageType::StaticClass()))
	{
		if (Instance->PlayerHealth->CurrentHealth == Instance->PlayerHealth->MaxHealth) return 0.f;
		if (Instance->PlayerHealth->CurrentHealth + Damage > Instance->PlayerHealth->MaxHealth)
		{
			float appliedDamage = Instance->PlayerHealth->MaxHealth - Instance->PlayerHealth->CurrentHealth;
			Instance->PlayerHealth->CurrentHealth = Instance->PlayerHealth->MaxHealth;
			return appliedDamage;
		}
		Instance->PlayerHealth->CurrentHealth = Instance->PlayerHealth->CurrentHealth + Damage;
		return Damage;
	}
	else if (DamageEvent.DamageTypeClass->IsChildOf(UDamageType::StaticClass()))
	{
		if (Instance->PlayerHealth->CurrentHealth <= 0.f) return 0.f;
		Instance->PlayerHealth->CurrentHealth = Instance->PlayerHealth->CurrentHealth - Damage;
		return Damage;
	}*/
	return 0.f;
}

void ACPPProjectCharacter::ToggleMainMenu()
{
	if (IsPaused)
	{
		IsPaused = false;
		
		UGameplayStatics::SetGamePaused(GetWorld(), IsPaused);
		if (GameHUD->IsInGameMenu) GameHUD->IsInGameMenu = false;		
	}
	else
	{
		APlayerController* PC = Cast<APlayerController>(GetController());
		if (PC)
		{
			if (GameHUD == NULL) GameHUD = Cast<AGameHUD>(PC->GetHUD());
			
			GameHUD->IsInGameMenu = true;
			IsPaused = true;
			UGameplayStatics::SetGamePaused(GetWorld(), IsPaused);
		}
	}
}

void ACPPProjectCharacter::Tick(float DeltaTime)
{
	LookForEnemies();
}

void ACPPProjectCharacter::LookForEnemies()
{
	for (int i = 0; i < ListOfEnemiesInRange.Num(); i++)
	{
		ListOfEnemiesInRange[i]->IsWatching = CheckTrace(ListOfEnemiesInRange[i]);
	}

}

bool ACPPProjectCharacter::CheckTrace(AActor* endPoint)
{
	FCollisionQueryParams TraceParams = FCollisionQueryParams(FName(TEXT("Trace")), true, this);
	TraceParams.bTraceComplex = true;
	TraceParams.bTraceAsyncScene = true;
	TraceParams.bReturnPhysicalMaterial = false;

	TArray<FHitResult> TraceHits;

	FVector StartVector = this->FollowCamera->GetComponentLocation();
	FVector EndVector = endPoint->GetActorLocation();


	GetWorld()->LineTraceMultiByChannel(TraceHits, StartVector, EndVector, ECC_Pawn, TraceParams);

	bool result = true;

	for (int i = 0; i < TraceHits.Num(); i++)
	{
		if (TraceHits[i].Actor == endPoint)
			return result;
		if (TraceHits[i].Actor->IsA(AEnemyBaseClass::StaticClass()))
			continue;
		if (TraceHits[i].Actor->IsA(ACPPProjectCharacter::StaticClass()))
			continue;

		result = false;
	}
	return result;	
}


void ACPPProjectCharacter::SendPressedKeyToHUD(FKey key)
{
	APlayerController* PC = Cast<APlayerController>(GetController());
	if (PC)
	{
		if (GameHUD == NULL) GameHUD = Cast<AGameHUD>(PC->GetHUD());
		GameHUD->SendKeyToActionBar(key);
	}
}

void ACPPProjectCharacter::CreateActorToActorProjectile()
{
	if (GetCharacterMovement()->IsMovementInProgress() || GetCharacterMovement()->IsFalling()) return;
	if (!Instance) Instance = Cast < UMainGameInstance>(GetGameInstance());
	Instance->SpellCastingManager->StartCasting("Frostbolt");
	/*if (!Instance->PlayerTarget) return;
	if (!Instance->PlayerTarget->isPlayerInRange) return;
	if (!CheckTrace(Instance->PlayerTarget)) return;
	FVector location = this->GetActorLocation();
	AActor* result = GetWorld()->SpawnActor<AFrostboltSpellProjectile>(FrostboltClass, location, FRotator(0,0,0), FActorSpawnParameters());*/
}
