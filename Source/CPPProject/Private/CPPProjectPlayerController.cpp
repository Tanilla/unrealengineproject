// Fill out your copyright notice in the Description page of Project Settings.

#include "CPPProject.h"
#include "CPPProjectPlayerController.h"


void ACPPProjectPlayerController::RightMouseButtonPressed()
{
	FHitResult result;
	bool value = GetHitResultUnderCursorByChannel(TraceTypeQuery1, true, result);
	if (value && result.Actor->IsA(AEnemyBaseClass::StaticClass()))
	{
		AEnemyBaseClass* enemy = Cast<AEnemyBaseClass>(result.GetActor());
		if (!enemy->IsAlive)
		{
			enemy->RightMouseButtonPressed.Broadcast();
		}
	}
	else if (value && result.Actor->IsA(UUserWidget::StaticClass()))
	{
		ULootWidgetClass* widget = Cast<ULootWidgetClass>(result.GetActor());
		if (!widget) return;
		else
		{
			FInputModeGameAndUI gameMode;
			gameMode.SetWidgetToFocus(widget->GetCachedWidget());
			SetInputMode(gameMode);
		}
	}
	else if (value)
	{
		ACPPProjectCharacter* character = Cast<ACPPProjectCharacter>(GetPawn());
		character->RightMouseButtonPressed();
	}
}

void ACPPProjectPlayerController::RightMouseButtonReleased()
{
	FHitResult result;
	bool value = GetHitResultUnderCursorByChannel(TraceTypeQuery1, true, result);
	if (value && result.Actor->IsA(AEnemyBaseClass::StaticClass()))
	{
		AEnemyBaseClass* enemy = Cast<AEnemyBaseClass>(result.GetActor());
		if (!enemy->IsAlive)
		{
			enemy->RightMouseButtonReleased.Broadcast();
		}
	}
	else if (value)
	{
		ACPPProjectCharacter* character = Cast<ACPPProjectCharacter>(GetPawn());
		character->RightMouseButtonReleased();
	}
}

