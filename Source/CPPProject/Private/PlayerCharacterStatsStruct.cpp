// Fill out your copyright notice in the Description page of Project Settings.

#include "CPPProject.h"
#include "PlayerCharacterStatsStruct.h"


UPlayerCharacterStatsStruct::UPlayerCharacterStatsStruct(const FObjectInitializer &ObjectInitializer)
: Super(ObjectInitializer)
{
	Stamina = 1;
	Strength = 1;
	Intellect = 1;
	Agility = 1;
	Charisma = 1;
	Luck = 1;
}

