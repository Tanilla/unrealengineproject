// Fill out your copyright notice in the Description page of Project Settings.

#include "CPPProject.h"
#include "GameHUD.h"
#include "EnemyBaseClass.h"

AEnemyBaseClass::AEnemyBaseClass()
{
	Health = FHealthResourceStruct();
	Health.CurrentHealth = Health.MaxHealth;
	LastHitImpactVector = FVector(-1, -1, -1);
	EnemyName = FText::FromString("Such Name, Much Enemy");
	RightMouseButtonReleased.AddDynamic(this, &AEnemyBaseClass::ShowLootWidget);
}

void AEnemyBaseClass::BeginPlay()
{
	Super::BeginPlay();
}


void AEnemyBaseClass::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );	

	if (!IsAlive && LootArray.Num() == 0)
	{

	}

	if (!HealthWidget && IsAlive)
	{
		if (!instance) instance = Cast<UMainGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
		HealthWidget = CreateWidget<UEnemyHealthWidgetClass>(instance, HealthWidgetClass);
		HealthWidget->enemy = this;
	}

	//Check if this enemy is on the screen right now
	FVector2D ScreenLocation;
	FVector RootLocation = AActor::GetActorLocation(RootComponent);	
	RootLocation += FVector(0, 0, 80);

	bool isInScreen = GetWorld()->GetFirstPlayerController()->ProjectWorldLocationToScreen(RootLocation, ScreenLocation);
	
	//if it is, and also if it is in player range and on the line of sight, then we add widget to the viewport
	if (isInScreen && isPlayerInRange && IsWatching && IsAlive)
	{
		HealthWidget->SetVisibility(ESlateVisibility::HitTestInvisible);
		HealthWidget->SetAlignmentInViewport(FVector2D(0.5, 0.5));
		//funny try to make widget scale. Widget become bigger then player comes closer.
		float x = 1 - (AActor::GetDistanceTo(GetWorld()->GetFirstPlayerController()->GetPawn()) / 2500);
		FVector2D widgetScale = FVector2D(x, x);
		HealthWidget->SetRenderScale(widgetScale);
		ScreenLocation -= FVector2D(0, 50 * x);
		HealthWidget->SetPositionInViewport(ScreenLocation);				
		HealthWidget->AddToViewport();
	}
	else
		HealthWidget->SetVisibility(ESlateVisibility::Hidden);

}


void AEnemyBaseClass::SelectAsPlayerTarget()
{
	//Change current player target - set new enemy as target at GameInstance and set target circle visible for selected enemy
	if (!instance) instance = Cast<UMainGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
	if (instance->PlayerTarget && instance->PlayerTarget != this)
	{
		instance->PlayerTarget->isSelected = false;
		instance->PlayerTarget->TargetCircle->SetHiddenInGame(true);
	}
	instance->PlayerTarget = this;
	isSelected = true;
	TargetCircle->SetHiddenInGame(false);
}


float AEnemyBaseClass::TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	if (!instance) instance = Cast<UMainGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
	Health.CurrentHealth -= Damage;

	LastHitImpactVector = DamageCauser->GetActorForwardVector();
	
	//Enemy with health under 0 - is dead enemy. Adda player experience for killing and destroys dead body
	if (Health.CurrentHealth <= 0)
	{
		IsAlive = false;
		if(instance->PlayerTarget == this) instance->PlayerTarget = nullptr;
		instance->GainExperience(25, false);
		LootArray = instance->LootGenerationManager->GenerateLootList(3);
		TargetCircle->SetHiddenInGame(true);
		GetCapsuleComponent()->SetCollisionResponseToAllChannels(ECR_Ignore);
		
		GetMesh()->SetCollisionResponseToChannel(ECollisionChannel::ECC_Visibility, ECR_Ignore);

		HealthWidget->RemoveFromViewport();
		GetWorldTimerManager().SetTimer(handle, this, &AEnemyBaseClass::TimerHandle_LifeExpired, 40, false);
	}
	return Damage;
}

void AEnemyBaseClass::TimerHandle_LifeExpired()
{
	EnemyDespawned.Broadcast();
	this->Destroy();
	GetWorldTimerManager().ClearTimer(handle);
}

void AEnemyBaseClass::StartGlowing()
{
	if (LootGlowParticle)
	{
		UGameplayStatics::SpawnEmitterAttached(LootGlowParticle, GetMesh(), "", FVector(0, 0, 0), FRotator(0, 0, 0), EAttachLocation::KeepWorldPosition, true);
		GetMesh()->SetCollisionResponseToChannel(ECollisionChannel::ECC_Visibility, ECR_Block);
	}}

void AEnemyBaseClass::ShowLootWidget()
{		
	AGameHUD* hud = Cast<AGameHUD>(GetWorld()->GetFirstPlayerController()->GetHUD());
	hud->SetLootWindowActor(this);
}
