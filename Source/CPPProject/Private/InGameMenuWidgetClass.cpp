// Fill out your copyright notice in the Description page of Project Settings.

#include "CPPProject.h"
#include "CPPProjectCharacter.h"
#include "GameHUD.h"
#include "InGameMenuWidgetClass.h"


void UInGameMenuWidgetClass::OnResumeGame_Click()
{
	//Hides this widget and tells other classes to restore settings(isPaused and MouseEvents) back.

	ACPPProjectCharacter* character = Cast<ACPPProjectCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
	this->RemoveFromParent();  
	OnMainMenuWidgetClosed.Broadcast(); 
	character->ToggleMainMenu();
}

void UInGameMenuWidgetClass::OnSettings_Click()
{
	//Open Settings Window and hides this one

	if (!instance) instance = Cast<UMainGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
	SettingsWindow = CreateWidget<USettingsWindowWidgetClass>(instance, SettingsWindowClass);
	SettingsWindow->AddToViewport();
	this->SetVisibility(ESlateVisibility::Hidden);
	SettingsWindow->OnWidgetClosed.AddDynamic(this, &UInGameMenuWidgetClass::SetWidgetVisible);
}

void UInGameMenuWidgetClass::OnExitToWindows_Click()
{
	//Closes the game. Bye-bye!

	if (!instance) instance = Cast<UMainGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
	UGameViewportClient* viewport = instance->GetGameViewportClient();
	if (viewport)
	{
		viewport->ConsoleCommand("quit");
	}
}

void UInGameMenuWidgetClass::OnSaveGame_Click()
{
	//Open SaveLoadWindow and hides this one. Also sends to SaveLoadWindow 'token' that it's saving, not loading

	if (!instance) instance = Cast<UMainGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
	SaveLoadWindow = CreateWidget<USaveLoadGameWidgetClass>(instance, SaveLoadWindowClass);	
	SaveLoadWindow->SetWindowStatus(true);
	SaveLoadWindow->AddToViewport();
	this->SetVisibility(ESlateVisibility::Hidden);
	SaveLoadWindow->OnWidgetClosed.AddDynamic(this, &UInGameMenuWidgetClass::SetWidgetVisible);
}

void UInGameMenuWidgetClass::OnLoadGame_Click()
{
	//Open SaveLoadWindow and hides this one. Also sends to SaveLoadWindow 'token' that it's loading, not saving

	if (!instance) instance = Cast<UMainGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
	SaveLoadWindow = CreateWidget<USaveLoadGameWidgetClass>(instance, SaveLoadWindowClass);
	SaveLoadWindow->SetWindowStatus(false);
	SaveLoadWindow->AddToViewport();
	this->SetVisibility(ESlateVisibility::Hidden);
	SaveLoadWindow->OnWidgetClosed.AddDynamic(this, &UInGameMenuWidgetClass::SetWidgetVisible);
}

void UInGameMenuWidgetClass::SetWidgetVisible()
{
	this->SetVisibility(ESlateVisibility::Visible);
}


