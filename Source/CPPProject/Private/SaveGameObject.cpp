// Fill out your copyright notice in the Description page of Project Settings.

#include "CPPProject.h"
#include "SaveGameObject.h"



void USaveGameObject::SetWorld(UWorld* currentWorld)
{
	world = currentWorld;
}

void USaveGameObject::CollectAllData()
{
	//collects data from all classes around this project to save it.

	instance = Cast<UMainGameInstance>(UGameplayStatics::GetGameInstance(world));
	ACPPProjectCharacter* character = Cast<ACPPProjectCharacter>(world->GetFirstPlayerController()->GetPawn());
	
	PlayerLocation = character->GetCapsuleComponent()->GetComponentLocation();
	PlayerRotation = character->GetCapsuleComponent()->GetComponentRotation();
	CameraRotation = world->GetFirstPlayerController()->GetControlRotation();
	
	PlayerLevel = instance->PlayerLevel->Level;
	PlayerCurrentExperience = instance->PlayerLevel->CurrentExperience;
	PlayerRequiredExperience = instance->PlayerLevel->RequiredExperience;

    
	PlayerHealth = 	instance->PlayerHealth.CurrentHealth;
}

void USaveGameObject::LoadAllData()
{
	//restores values to their variables 

	instance = Cast<UMainGameInstance>(UGameplayStatics::GetGameInstance(world));
	ACPPProjectCharacter* character = Cast<ACPPProjectCharacter>(world->GetFirstPlayerController()->GetPawn());

	world->GetFirstPlayerController()->SetControlRotation(CameraRotation);
	character->GetCapsuleComponent()->SetWorldLocationAndRotation(PlayerLocation, PlayerRotation);
	world->GetFirstPlayerController()->GetPawn()->SetActorLocationAndRotation(PlayerLocation, PlayerRotation);
	
	instance->PlayerLevel->Level = PlayerLevel;
	instance->PlayerLevel->CurrentExperience = PlayerCurrentExperience;
	instance->PlayerLevel->RequiredExperience = PlayerRequiredExperience;

	instance->PlayerHealth.CurrentHealth;
}

