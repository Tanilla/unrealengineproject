// Fill out your copyright notice in the Description page of Project Settings.

#include "CPPProject.h"
#include "ErrorMessagesWidgetClass.h"

void UErrorMessagesWidgetClass::NativeConstruct()
{
	Super::NativeConstruct();
	MessagesArray.AddDefaulted(5);
	Timers.AddZeroed(5);
}

void UErrorMessagesWidgetClass::AddNewMessage(FText newMessage)
{
	if (!MessagesArray[4].IsEmpty())
	{
		GetWorld()->GetTimerManager().ClearTimer(Timers[4]);
	}
	for (int i = 3; i >= 0; i--)
	{
		if (MessagesArray[i].IsEmpty()) continue;
		MessagesArray[i + 1] = MessagesArray[i];
		Timers[i + 1] = Timers[i];
 	}
	MessagesArray[0] = newMessage;
	FTimerHandle handle;
	GetWorld()->GetTimerManager().SetTimer(handle, this, &UErrorMessagesWidgetClass::RemoveMessageOnTimer, 1.0f, false, 5);
	Timers[0] = handle;
}

void UErrorMessagesWidgetClass::RemoveMessageOnTimer()
{
	for (int i = 4; i >= 0; i--)
	{
		if (MessagesArray[i].IsEmpty()) continue;
		MessagesArray[i] = FText::GetEmpty();
		break;
	}
	GetWorld()->GetTimerManager().ClearTimer(Timers[4]);
}


