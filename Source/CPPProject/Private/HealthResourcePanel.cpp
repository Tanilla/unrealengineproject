// Fill out your copyright notice in the Description page of Project Settings.

#include "CPPProject.h"
#include "HealthResourcePanel.h"

float UHealthResourcePanel::HealthPercent(UMainGameInstance* instance)
{
	return (float)instance->PlayerHealth.CurrentHealth / (float)instance->PlayerHealth.MaxHealth;
}

float UHealthResourcePanel::ResourcePercent(UMainGameInstance* instance)
{
	return (float)instance->PlayerHealth.CurrentResource / (float)instance->PlayerHealth.MaxResource;	
}

FString UHealthResourcePanel::GetHeroName(UMainGameInstance* instance)
{
	FString HeroName = instance->PlayerDefinition->Name;
	return HeroName;
}