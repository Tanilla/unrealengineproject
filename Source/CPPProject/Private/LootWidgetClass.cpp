// Fill out your copyright notice in the Description page of Project Settings.

#include "CPPProject.h"
#include "EnemyBaseClass.h"
#include "LootWidgetClass.h"


void ULootWidgetClass::CreateItemsList()
{
	if (!gridPanel) return;
	AEnemyBaseClass* enemyClassObject = Cast<AEnemyBaseClass>(enemyObject);
	if (enemyClassObject->LootArray.Num() == 0) return;

	for (int32 i = 0; i < enemyClassObject->LootArray.Num(); i++)
	{
		ULootItemWidgetClass* LootItemWidget = CreateWidget<ULootItemWidgetClass>(enemyObject->GetGameInstance(), LootItemWidgetClass);
		LootItemWidget->SetLootItem(enemyClassObject->LootArray[i]);
		UGridSlot* widgetSlot = gridPanel->AddChildToGrid(LootItemWidget);
		widgetSlot->Row = i + 1;
		widgetSlot->ColumnSpan = IsScrollNeeded() ? 1 : 2;
		widgetSlot->Column = 0;
	}
}

bool ULootWidgetClass::IsScrollNeeded()
{
	AEnemyBaseClass* enemy = Cast<AEnemyBaseClass>(enemyObject);
	if (enemy)
	{
		if (enemy->LootArray.Num() > 6)
			return true;
	}
	return false;
}

void ULootWidgetClass::SetEnemyObject(AActor* enemy)
{
	AEnemyBaseClass* enemyClassObject = Cast<AEnemyBaseClass>(enemyObject);
	if (enemyClassObject && enemyClassObject->EnemyDespawned.IsAlreadyBound(this, &ULootWidgetClass::RemoveFromParent))
		enemyClassObject->EnemyDespawned.RemoveDynamic(this, &ULootItemWidgetClass::RemoveFromParent);
	
	enemyObject = enemy;
	enemyClassObject = Cast<AEnemyBaseClass>(enemyObject);

	ContainerName = enemyClassObject->EnemyName;
	enemyClassObject->EnemyDespawned.AddDynamic(this, &ULootWidgetClass::RemoveFromParent);
	ClearItems();
	CreateItemsList();
}

void ULootWidgetClass::OnMouseLeave(const FPointerEvent& MouseEvent)
{
	FSlateApplication::Get().SetUserFocusToGameViewport(0);
}

void ULootWidgetClass::Destruct()
{
	FSlateApplication::Get().SetUserFocusToGameViewport(0);
}

void ULootWidgetClass::ClearItems()
{
	if (!gridPanel) return;
	if (gridPanel->GetChildrenCount() <= 2) return;
	
	int32 currentElement = 0;
	while (gridPanel->GetChildrenCount() > 2)
	{
		if (currentElement > gridPanel->GetChildrenCount()) break;
		UWidget* widget = gridPanel->GetChildAt(currentElement);
		if (widget->IsA(ULootItemWidgetClass::StaticClass()))
		{
			gridPanel->RemoveChildAt(currentElement);
		}
		else
			currentElement++;
	}
}

void ULootWidgetClass::TakeAll()
{
	AEnemyBaseClass* enemyClassObject = Cast<AEnemyBaseClass>(enemyObject);
	enemyClassObject->LootArray.Empty();
	ClearItems();
}