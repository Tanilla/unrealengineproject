// Fill out your copyright notice in the Description page of Project Settings.

#include "CPPProject.h"
#include "FrostboltSpellProjectile.h"


// Sets default values
AFrostboltSpellProjectile::AFrostboltSpellProjectile()
{
	PrimaryActorTick.bCanEverTick = true;
}

void AFrostboltSpellProjectile::BeginPlay()
{
	Super::BeginPlay();	
}

void AFrostboltSpellProjectile::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );	
}

