// Fill out your copyright notice in the Description page of Project Settings.

#include "CPPProject.h"
#include "SettingsWindowWidgetClass.h"


void USettingsWindowWidgetClass::OnCancel_Click()
{
	if(this)
		this->RemoveFromParent();
	OnWidgetClosed.Broadcast();
}


void USettingsWindowWidgetClass::OnAccept_Click()
{	
	if (!VideoSettingTab) return;
	if (!settings)
		settings = GEngine->GetGameUserSettings();

	SetWindowMode();
	SetResolution();	
	SetVideoSetting(VideoSettingTab->currentEffectQuality, ESettingType::ST_Effects_Quality);
	SetVideoSetting(VideoSettingTab->currentShadowQuality, ESettingType::ST_Shadow_Quality);
	SetVideoSetting(VideoSettingTab->currentTextureQuality, ESettingType::ST_Texture_Quality);
	SetVideoSetting(VideoSettingTab->currentViewDistance, ESettingType::ST_View_Distance);

	settings->RequestResolutionChange(settings->GetScreenResolution().X, settings->GetScreenResolution().Y, settings->GetFullscreenMode());

	if (FadingWidgetClass)
	{
		if (!FadingWidget)
		{
			FadingWidget = CreateWidget<UUserWidget>(Cast<UMainGameInstance>(UGameplayStatics::GetGameInstance(GetWorld())), FadingWidgetClass);
		}

		FadingWidget->AddToViewport();
	}

	if (MessageBoxClass)
	{
		if (!MessageBoxWidget)
		{
			MessageBoxWidget = CreateWidget<UTimerMessageBoxWidgetClass>(Cast<UMainGameInstance>(UGameplayStatics::GetGameInstance(GetWorld())), MessageBoxClass);
			MessageBoxWidget->OnMessageBoxClosed.AddDynamic(VideoSettingTab, &UVideoTabSettingsWidgetClass::BeginPlay);
			MessageBoxWidget->OnMessageBoxClosed.AddDynamic(this, &USettingsWindowWidgetClass::RemoveFading);
		}

		MessageBoxWidget->AddToViewport();
	}

}


void USettingsWindowWidgetClass::SetWindowMode()
{
	switch (VideoSettingTab->currentWindowMode)
	{
	case EWindowModeCustom::WMC_Borderless:
		settings->SetFullscreenMode(EWindowMode::WindowedFullscreen);
		break;
	case EWindowModeCustom::WMC_Windowed:
		settings->SetFullscreenMode(EWindowMode::Windowed);
		break;
	case EWindowModeCustom::WMC_Fullscreen:
		settings->SetFullscreenMode(EWindowMode::Fullscreen);
		break;
	}
}

void USettingsWindowWidgetClass::SetResolution()
{
	switch (VideoSettingTab->currentResolution)
	{
	case EVideoResolution::VR_1024_768:
		settings->SetScreenResolution(FIntPoint(1024, 768));
		break;
	case EVideoResolution::VR_1280_1024:
		settings->SetScreenResolution(FIntPoint(1280, 1024));
		break;
	case EVideoResolution::VR_1280_800:
		settings->SetScreenResolution(FIntPoint(1280, 800));
		break;
	case EVideoResolution::VR_1366_768:
		settings->SetScreenResolution(FIntPoint(1366, 768));
		break;
	case EVideoResolution::VR_1440_900:
		settings->SetScreenResolution(FIntPoint(1440, 900));
		break;
	case EVideoResolution::VR_1600_900:
		settings->SetScreenResolution(FIntPoint(1600, 900));
		break;
	case EVideoResolution::VR_1680_1050:
		settings->SetScreenResolution(FIntPoint(1680, 1050));
		break;
	case EVideoResolution::VR_1920_1080:
		settings->SetScreenResolution(FIntPoint(1920, 1080));
		break;
	case EVideoResolution::VR_2560_1440:
		settings->SetScreenResolution(FIntPoint(2560, 1440));
		break;
	}
}

void USettingsWindowWidgetClass::RemoveFading()
{
	FadingWidget->RemoveFromParent();
}

void USettingsWindowWidgetClass::SetVideoSetting(EVideoSettings videoSetting, ESettingType type)
{
	int32 quality = 0;
	switch (videoSetting)
	{
	case EVideoSettings::VS_Low:
		quality = 0;
		break;
	case EVideoSettings::VS_Medium:
		quality = 1;
		break;
	case EVideoSettings::VS_High:
		quality = 2;
		break;
	case EVideoSettings::VS_VeryHigh:
		quality = 3;
		break;
	}
	
	switch (type)
	{
	case ESettingType::ST_Effects_Quality:
		settings->ScalabilityQuality.EffectsQuality = quality;
		break;
	case ESettingType::ST_Shadow_Quality:
		settings->ScalabilityQuality.ShadowQuality = quality;
		break;
	case ESettingType::ST_Texture_Quality:
		settings->ScalabilityQuality.TextureQuality = quality;
		break;
	case ESettingType::ST_View_Distance:
		settings->ScalabilityQuality.ViewDistanceQuality = quality;
		break;
	}
}