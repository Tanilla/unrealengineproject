// Fill out your copyright notice in the Description page of Project Settings.

#include "CPPProject.h"
#include "TimerMessageBoxWidgetClass.h"

#define LOCTEXT_NAMESPACE "CPPProject"

#undef LOCTEXT_NAMESPACE

void UTimerMessageBoxWidgetClass::NativeConstruct()
{
	Super::NativeConstruct();
	currentCounter = 20;
}

void UTimerMessageBoxWidgetClass::NativeTick(const FGeometry &MyGeometry, float InDeltaTime)
{
	Super::NativeTick(MyGeometry, InDeltaTime);
	if (currentCounter >= 0 && this->IsInViewport())
		Timer(InDeltaTime);
}

void UTimerMessageBoxWidgetClass::YesButtonClick()
{
	if (!settings)
		settings = GEngine->GetGameUserSettings();

	settings->ConfirmVideoMode();
	settings->ApplyResolutionSettings(true);
	settings->ApplySettings(true);

	OnMessageBoxClosed.Broadcast();
	this->RemoveFromParent();
}

void UTimerMessageBoxWidgetClass::NoButtonClick()
{
	if (!settings)
		settings = GEngine->GetGameUserSettings();

	settings->RevertVideoMode();
	settings->ApplyResolutionSettings(true);
	settings->ApplySettings(true);

	OnMessageBoxClosed.Broadcast();
	this->RemoveFromParent();
}

FText UTimerMessageBoxWidgetClass::GetMessageBoxText()
{
	FNumberFormattingOptions numberFormat;
	numberFormat.MinimumFractionalDigits = 0;
	numberFormat.MaximumFractionalDigits = 0;
	return FText::Format(NSLOCTEXT("CPPProject","MessageBoxText", "The video settings have been changed. \nKeep these settings?\n They will be reverted in {0} seconds"), FText::AsNumber(currentCounter, &numberFormat));
}


void UTimerMessageBoxWidgetClass::Timer(float delta)
{
	currentCounter -= delta;
	if (currentCounter <= 0)
	{
		NoButtonClick();
	}
}