// Fill out your copyright notice in the Description page of Project Settings.

#include "CPPProject.h"
#include "VideoTabSettingsWidgetClass.h"


void UVideoTabSettingsWidgetClass::BeginPlay()
{
	UGameUserSettings* settings = GEngine->GetGameUserSettings();
	
	currentTextureQuality = (EVideoSettings)settings->ScalabilityQuality.TextureQuality;
	currentShadowQuality = (EVideoSettings)settings->ScalabilityQuality.ShadowQuality;
	currentEffectQuality = (EVideoSettings)settings->ScalabilityQuality.EffectsQuality;
	currentViewDistance = (EVideoSettings)settings->ScalabilityQuality.ViewDistanceQuality;


	switch (settings->GetFullscreenMode())
	{
	case EWindowMode::Fullscreen:
		currentWindowMode = EWindowModeCustom::WMC_Fullscreen;
		break;
	case EWindowMode::Windowed:
		currentWindowMode = EWindowModeCustom::WMC_Windowed;
		break;
	case EWindowMode::WindowedFullscreen:
		currentWindowMode = EWindowModeCustom::WMC_Borderless;
		break;
	}

	FIntPoint screenResolition = settings->GetScreenResolution();

	//Doesn't look good...

	if (screenResolition.X == 1024 && screenResolition.Y == 768)
		currentResolution = EVideoResolution::VR_1024_768;
	else if (screenResolition.X == 1280 && screenResolition.Y == 800)
		currentResolution = EVideoResolution::VR_1280_800;
	else if (screenResolition.X == 1280 && screenResolition.Y == 1024)
		currentResolution = EVideoResolution::VR_1280_1024;
	else if (screenResolition.X == 1366 && screenResolition.Y == 768)
		currentResolution = EVideoResolution::VR_1366_768;
	else if (screenResolition.X == 1440 && screenResolition.Y == 900)
		currentResolution = EVideoResolution::VR_1440_900;
	else if (screenResolition.X == 1600 && screenResolition.Y == 900)
		currentResolution = EVideoResolution::VR_1600_900;
	else if (screenResolition.X == 1680 && screenResolition.Y == 1050)
		currentResolution = EVideoResolution::VR_1680_1050;
	else if (screenResolition.X == 1920 && screenResolition.Y == 1080)
		currentResolution = EVideoResolution::VR_1920_1080;
	else if (screenResolition.X == 2560 && screenResolition.Y == 1440)
		currentResolution = EVideoResolution::VR_2560_1440;
	else
		currentResolution = EVideoResolution::VR_1024_768;

}

void UVideoTabSettingsWidgetClass::SetAmounts()
{
	const UEnum* enumVRPtr = FindObject<UEnum>(ANY_PACKAGE, TEXT("EVideoResolution"), true);
	resolutionsAmount = enumVRPtr->NumEnums();

	const UEnum* enumVSPtr = FindObject<UEnum>(ANY_PACKAGE, TEXT("EVideoSettings"), true);
	settingsAmount = enumVSPtr->NumEnums();

	const UEnum* enumWMPtr = FindObject<UEnum>(ANY_PACKAGE, TEXT("EWindowModeCustom"), true);
	windowAmount = enumWMPtr->NumEnums();
}

void UVideoTabSettingsWidgetClass::GetNext(ESettingType blockName)
{
	if (resolutionsAmount == 0 || settingsAmount == 0 || windowAmount == 0)
		SetAmounts();

	switch (blockName)
	{
	case ESettingType::ST_Effects_Quality:
		if ((uint8)currentEffectQuality + 1 < settingsAmount - 1)
			currentEffectQuality = (EVideoSettings)((uint8)currentEffectQuality + 1);
		break;
	case ESettingType::ST_Resolution:
		if ((uint8)currentResolution + 1 < resolutionsAmount - 1)
			currentResolution = (EVideoResolution)((uint8)currentResolution + 1);
		break;
	case ESettingType::ST_Shadow_Quality:
		if ((uint8)currentShadowQuality + 1 < settingsAmount - 1)
			currentShadowQuality = (EVideoSettings)((uint8)currentShadowQuality + 1);
		break;
	case ESettingType::ST_Texture_Quality:
		if ((uint8)currentTextureQuality + 1 < settingsAmount - 1)
			currentTextureQuality = (EVideoSettings)((uint8)currentTextureQuality + 1);
		break;
	case ESettingType::ST_View_Distance:
		if ((uint8)currentViewDistance + 1 < settingsAmount - 1)
			currentViewDistance = (EVideoSettings)((uint8)currentViewDistance + 1);
		break;
	case ESettingType::ST_Window_Mode:
		if ((uint8)currentWindowMode + 1 < windowAmount - 1)
			currentWindowMode = (EWindowModeCustom)((uint8)currentWindowMode + 1);
		break;
	}	
}

void UVideoTabSettingsWidgetClass::GetPrevious(ESettingType blockName)
{
	switch (blockName)
	{
	case ESettingType::ST_Effects_Quality:
		if ((uint8)currentEffectQuality - 1 >= 0)
			currentEffectQuality = (EVideoSettings)((uint8)currentEffectQuality - 1);
		break;
	case ESettingType::ST_Resolution:
		if ((uint8)currentResolution - 1 >= 0)
			currentResolution = (EVideoResolution)((uint8)currentResolution - 1);
		break;
	case ESettingType::ST_Shadow_Quality:
		if ((uint8)currentShadowQuality - 1 >= 0)
			currentShadowQuality = (EVideoSettings)((uint8)currentShadowQuality - 1);
		break;
	case ESettingType::ST_Texture_Quality:
		if ((uint8)currentTextureQuality - 1 >= 0)
			currentTextureQuality = (EVideoSettings)((uint8)currentTextureQuality - 1);
		break;
	case ESettingType::ST_View_Distance:
		if ((uint8)currentViewDistance - 1 >= 0)
			currentViewDistance = (EVideoSettings)((uint8)currentViewDistance - 1);
		break;
	case ESettingType::ST_Window_Mode:
		if ((uint8)currentWindowMode - 1 >= 0)
			currentWindowMode = (EWindowModeCustom)((uint8)currentWindowMode - 1);
		break;

	}
}

//Next three methods return the names of enum lines

FString UVideoTabSettingsWidgetClass::GetResolutionText(uint8 index)
{
	switch (index)
	{
	case 0:
		return TEXT("1024x768");
	case 1:
		return TEXT("1280x800");
	case 2:
		return TEXT("1280x1024");
	case 3:
		return TEXT("1366x768");
	case 4:
		return TEXT("1440x900");
	case 5:
		return TEXT("1600x900");
	case 6:
		return TEXT("1680x1050");
	case 7:
		return TEXT("1920x1080");
	case 8:
		return TEXT("2560x1440");
	}
	return TEXT("");
}

FString UVideoTabSettingsWidgetClass::GetQualityText(uint8 index)
{
	switch (index)
	{
	case 0:
		return TEXT("Low");
	case 1:
		return TEXT("Medium");
	case 2:
		return TEXT("High");
	case 3:
		return TEXT("Very High");
	}
	return TEXT("");
}

FString UVideoTabSettingsWidgetClass::GetWindowModeText(uint8 index)
{
	switch (index)
	{
	case 0:
		return TEXT("Fullscreen");
	case 1:
		return TEXT("Windowed");
	case 2:
		return TEXT("Borderless");
	}
	return TEXT("");
}