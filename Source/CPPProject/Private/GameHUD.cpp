// Fill out your copyright notice in the Description page of Project Settings.

#include "CPPProject.h"
#include "GameHUD.h"

AGameHUD::AGameHUD(const class FObjectInitializer& PCIP)
: Super(PCIP)
{

}

void AGameHUD::BeginPlay()
{
	Super::BeginPlay();

	//Instantiate base widgets and add them to viewport. Health and ActionBar has Z-Order so they will be above enemy's health widget.
	if (HealthResourcePanelClass)
	{
		if (!Instance) Instance = Cast<UMainGameInstance>(GetGameInstance());
		HealthResourcePanel = CreateWidget<UHealthResourcePanel>(Instance, HealthResourcePanelClass);
		HealthResourcePanel->AddToViewport(10);
	}	

	if (InGameMenuClass)
	{
		if (!InGameMenu)
		{
			if (!Instance) Instance = Cast<UMainGameInstance>(GetGameInstance());
			InGameMenu = CreateWidget<UInGameMenuWidgetClass>(Instance, InGameMenuClass);
			InGameMenu->OnMainMenuWidgetClosed.AddDynamic(this, &AGameHUD::SetGameMenusVisibility);
		}
	}

	if (ActionBarClass)
	{
		if (!Instance) Instance = Cast<UMainGameInstance>(GetGameInstance());
		ActionBar = CreateWidget<UActionBarWidgetClass>(Instance, ActionBarClass);
		ActionBar->AddToViewport(10);
	}

	if (ActionBarClass)
	{
		if (!Instance) Instance = Cast<UMainGameInstance>(GetGameInstance());
		ErrorMessagesWidget = CreateWidget<UErrorMessagesWidgetClass>(Instance, ErrorMessagesClass);
		ErrorMessagesWidget->AddToViewport(10);
	}
}

void AGameHUD::SetGameMenusVisibility()
{
	HealthResourcePanel->SetVisibility(ESlateVisibility::SelfHitTestInvisible);
	ActionBar->SetVisibility(ESlateVisibility::SelfHitTestInvisible);
	ErrorMessagesWidget->SetVisibility(ESlateVisibility::HitTestInvisible);
	LootWidget->SetVisibility(ESlateVisibility::HitTestInvisible);

	//restore focus on viewport, so you don't need to click mouse once before you get your mouse events fired again.
	APlayerController* PC = Cast<APlayerController>(GetWorld()->GetFirstPlayerController());
	if (PC)
	{
		FInputModeGameAndUI gameMode;
		PC->SetInputMode(gameMode);
		FSlateApplication::Get().SetUserFocusToGameViewport(0);
	}
}

void AGameHUD::DrawHUD()
{
	if (Instance == NULL) Instance = Cast<UMainGameInstance>(GetGameInstance());
	if (!IsInGameMenu)
	{
		Super::DrawHUD();
	}
	else
	{
		if (InGameMenu != nullptr)
		{
			if (!InGameMenu->IsInViewport())
			{
				APlayerController* PC = Cast<APlayerController>(GetWorld()->GetFirstPlayerController());
				if (PC)
				{
					InGameMenu->AddToViewport();
					FInputModeUIOnly mode;
					mode.SetWidgetToFocus(InGameMenu->GetCachedWidget());
					PC->SetInputMode(mode);
					HealthResourcePanel->SetVisibility(ESlateVisibility::Hidden);
					ActionBar->SetVisibility(ESlateVisibility::Hidden);
					ErrorMessagesWidget->SetVisibility(ESlateVisibility::Hidden);
					LootWidget->SetVisibility(ESlateVisibility::Hidden);
				}				
			}				
		}
		else IsInGameMenu = false;
	}

}

void AGameHUD::SendKeyToActionBar(FKey key)
{
	if (!ActionBar) return;
	ActionBar->KeyPressed(key);
}

void AGameHUD::SetLootWindowActor(AActor* lootActor)
{
	if (IsInGameMenu) return;
	if (!LootWidgetClass) return;
	if (LootWidget && LootWidget->IsInViewport() && LootWidget->enemyObject == lootActor) return;
	if (!LootWidget)
		LootWidget = CreateWidget<ULootWidgetClass>(Instance, LootWidgetClass);

	if (!LootWidget) return;
	if (LootWidget->enemyObject != lootActor)
		LootWidget->RemoveFromParent();
	LootWidget->SetEnemyObject(lootActor);
	LootWidget->AddToViewport();
	LootWidget->SetPositionInViewport(FVector2D(1000, 500));

	FSlateApplication::Get().SetUserFocusToGameViewport(0);
}

