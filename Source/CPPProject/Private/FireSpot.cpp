// Fill out your copyright notice in the Description page of Project Settings.

#include "CPPProject.h"
#include "CPPProjectCharacter.h"
#include "FireSpot.h"


// Sets default values
AFireSpot::AFireSpot()
{
	PrimaryActorTick.bCanEverTick = false;

	BaseCollisionComponent = Super::CreateDefaultSubobject<UBoxComponent>(TEXT("CollisionComponent")); //Create base sphere component

	BaseCollisionComponent->InitBoxExtent(FVector(80, 80, 80));
	BaseCollisionComponent->BodyInstance.SetCollisionProfileName("OverlapAll");
	BaseCollisionComponent->bGenerateOverlapEvents = true;
	BaseCollisionComponent->OnComponentBeginOverlap.AddDynamic(this, &AFireSpot::OnOverlapBegin);
	BaseCollisionComponent->OnComponentEndOverlap.AddDynamic(this, &AFireSpot::OnOverlapEnd);
	RootComponent = BaseCollisionComponent; //Set the Sphere as the root component

	FireWallEffect = Super::CreateDefaultSubobject<UParticleSystemComponent>(TEXT("FireWallEffect"));
	FireWallEffect->AttachTo(RootComponent);

	static ConstructorHelpers::FObjectFinder<UParticleSystem>ParticleSystem(TEXT("ParticleSystem'/Game/StarterContent/Particles/P_Fire.P_Fire'"));
	if (ParticleSystem.Object != NULL)
	{
		FireWallPS = ParticleSystem.Object;
		FireWallEffect->SetTemplate(ParticleSystem.Object);
	}


}

// Called when the game starts or when spawned
void AFireSpot::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFireSpot::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

void AFireSpot::OnOverlapBegin(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	//After player steps into the fire - he'll burn...
	if (instance == NULL) instance = Cast<UMainGameInstance>(GetGameInstance());
	if ((OtherActor != nullptr) && (OtherActor != this) && (OtherComp != nullptr))
	{
		if (OtherActor->IsA(ACPPProjectCharacter::StaticClass()))
		{
			GetWorldTimerManager().SetTimer(timerHandle, this, &AFireSpot::TimerTick, 1.0f, true, 0.0f);
		}
	}
}

void AFireSpot::OnOverlapEnd(class AActor* otherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	//...and clears after he'll go away. 
	GetWorldTimerManager().ClearTimer(timerHandle);
}

void AFireSpot::TimerTick()
{
	//Ouch! It hurts!
	if (instance->PlayerHealth.CurrentHealth > 0)
		instance->PlayerHealth.CurrentHealth -= 10;
}