// Fill out your copyright notice in the Description page of Project Settings.

#include "CPPProject.h"
#include "LootGenerationClass.h"

ULootGenerationClass::ULootGenerationClass(const FObjectInitializer &ObjectInitializer)
: Super(ObjectInitializer)
{
	ConstructorHelpers::FObjectFinder<UDataTable>ItemsDT(TEXT("DataTable'/Game/ThirdPerson/Data/ItemsDataTable.ItemsDataTable'"));
	if (ItemsDT.Object)
		ItemsDataTable = ItemsDT.Object;
}

TArray<FLootItemStruct> ULootGenerationClass::GenerateLootList(int32 maxNumberOfItems)
{
	TArray<FLootItemStruct> listOfItems;
	int32 MaxItemsWeight = 0;
	for(auto item : ItemsDataTable->RowMap)
	{
		FLootItemStruct* data = (FLootItemStruct*)(item.Value);
		if (data->RollWeight != 0)
			MaxItemsWeight += data->RollWeight;
	}

	for (int32 i = 0; i < maxNumberOfItems; i++)
	{
		int32 roll = FMath::RandRange(0, MaxItemsWeight);
		int32 leftWeightBorder = 0;
		for (auto item : ItemsDataTable->RowMap)
		{
			FLootItemStruct* data = (FLootItemStruct*)(item.Value);
			if (data->RollWeight == 0) continue;
			if (roll >= leftWeightBorder && roll <= leftWeightBorder + data->RollWeight)
			{
				listOfItems.Add(*data);
				break;
			}
			else
			{
				leftWeightBorder += data->RollWeight;
			}
		}
	}

	return listOfItems;
}
