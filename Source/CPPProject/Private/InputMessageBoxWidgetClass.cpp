// Fill out your copyright notice in the Description page of Project Settings.

#include "CPPProject.h"
#include "InputMessageBoxWidgetClass.h"


/* TODO:
*	- Check that name of save is appropriate
*/

void UInputMessageBoxWidgetClass::CancelSaving()
{
	dialogResult = false;
	SaveName = FText::FromString("");
	OnInputMessageBoxWidgetClosed.Broadcast();
	this->RemoveFromParent();
}

void UInputMessageBoxWidgetClass::SaveGame()
{
	dialogResult = true;
	OnInputMessageBoxWidgetClosed.Broadcast();
	this->RemoveFromParent();
}

