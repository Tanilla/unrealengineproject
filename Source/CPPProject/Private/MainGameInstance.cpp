// Fill out your copyright notice in the Description page of Project Settings.

#include "CPPProject.h"
#include "MainGameInstance.h"

UMainGameInstance::UMainGameInstance(const FObjectInitializer &ObjectInitializer)
: Super(ObjectInitializer)
{
	PlayerStats = NewObject<UPlayerCharacterStatsStruct>(UPlayerCharacterStatsStruct::StaticClass());
	PlayerDefinition = NewObject<UPlayerCharacterDefinitionStruct>(UPlayerCharacterDefinitionStruct::StaticClass());  
	PlayerLevel = NewObject<UPlayerLevelStruct>(UPlayerLevelStruct::StaticClass());  
	PlayerHealth = FHealthResourceStruct();
	SpellCastingManager = NewObject<USpellCastManagerClass>(USpellCastManagerClass::StaticClass());
	SpellCastingManager->instance = this;

	LootGenerationManager = NewObject<ULootGenerationClass>(ULootGenerationClass::StaticClass());

	static ConstructorHelpers::FObjectFinder<UParticleSystem>ParticleSystem(TEXT("ParticleSystem'/Game/ThirdPerson/Effects/FX_Ability/Heal/LevelUp.LevelUp'"));
	if (ParticleSystem.Object != NULL)
	{
		LevelUpPatricle = ParticleSystem.Object;
	}
}

void UMainGameInstance::GainExperience(int ExperienceAmount, bool IsQuestReward)
{
	//checks if player have enough experience to gain next level. If it's true then it calculates next level, experience above required one and plays 'New Level' animation

	if (PlayerLevel->CurrentExperience + ExperienceAmount >= PlayerLevel->RequiredExperience)
	{
		PlayerLevel->Level++;
		GEngine->AddOnScreenDebugMessage(-1, 10, FColor::Yellow, "Congratulations! You reached next level!");

		PlayerLevel->CurrentExperience = (PlayerLevel->CurrentExperience + ExperienceAmount) - PlayerLevel->RequiredExperience;
		PlayerLevel->RequiredExperience += 100 * PlayerLevel->Level;
		
		UGameplayStatics::SpawnEmitterAttached(LevelUpPatricle, GetWorld()->GetFirstPlayerController()->GetPawn()->GetRootComponent(), "", FVector(0, 0, 0), FRotator(0, 0, 0), EAttachLocation::KeepRelativeOffset, true);

		return;
	}
	PlayerLevel->CurrentExperience += ExperienceAmount;
}


