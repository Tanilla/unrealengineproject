﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "CPPProject.h"
#include "SaveListWidgetClass.h"



void USaveListWidgetClass::NativeConstruct()
{
	Super::NativeConstruct();
	firstLineId = 0;
	scrollCounter = baseScrollCounter;
	

	//Reads names of the files from save folder in user documents dir. Checks if there are enough save files to get full list(so we can scroll it) or not(and we can't)
	TArray<FString> directoriesToSkip;
	IPlatformFile &PlatformFile = FPlatformFileManager::Get().GetPlatformFile();
	FLocalTimestampDirectoryVisitor visitor = FLocalTimestampDirectoryVisitor(PlatformFile, directoriesToSkip, directoriesToSkip);
	FString directory = FPlatformProcess::UserDir();
	directory.Append("\\OurProject\\Saves\\");
	PlatformFile.IterateDirectory(*directory, visitor);

	for (TMap<FString, FDateTime>::TIterator TimestampIt(visitor.FileTimes); TimestampIt; ++TimestampIt)
	{
		SaveNameList.Add(FText::FromString(FPaths::GetBaseFilename(TimestampIt.Key())));
	}	

	int savesCounter = SaveNameList.Num() >= 10 ? 10 : SaveNameList.Num();

	for (int i = firstLineId; i < savesCounter; i++)
	{
		SaveSlotNames.Add(SaveNameList[i]);
	}


	if(SaveNameList.Num() >= 1)
		BorderColors.Add(FLinearColor(1, 0, 0, 0.85));
	else
		BorderColors.Add(FLinearColor(1, 1, 1));
	for (int i = 0; i < 9; i++)
	{
		BorderColors.Add(FLinearColor(1, 1, 1));
	}
}

FReply USaveListWidgetClass::NativeOnMouseWheel(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent)
{
	float delta = InMouseEvent.GetWheelDelta();
	if (delta > 0)
	{
		ScrollUp();
	}
	else
	{
		ScrollDown();
	}

	return FReply::Handled();
}

void USaveListWidgetClass::NativeTick(const FGeometry &MyGeometry, float InDeltaTime)
{
	//tricky one. It's about 'pressing' borders(ScrollButtons). If mouse left(it's not hovered) then we should stop scrolling. 
	//Also checks timer - should we scroll again or not. Can't use timer here 'cause it's not being fired when game is paused

	if (UpButtonBorder && DownButtonBorder)
	{
		if (isUpButtonPressed && UpButtonBorder->IsHovered() == false)
			isUpButtonPressed = false;
		if (isDownButtonPressed && DownButtonBorder->IsHovered() == false)
			isDownButtonPressed = false;
	}

	if (!isUpButtonPressed && !isDownButtonPressed) return;
	scrollCounter -= InDeltaTime;
	if (scrollCounter <= 0)
	{
		if (isUpButtonPressed)
			ScrollUp();
		else
			ScrollDown();

		isScrolledAtLeastOnce = true;
		scrollCounter = baseScrollCounter;
	}
}

void USaveListWidgetClass::SelectSaveRow(int32 BorderId)
{
	if (SaveSlotNames.Num() <= BorderId || SaveSlotNames[BorderId].EqualTo(FText::FromString(""))) return;
	for (int i = 0; i < BorderColors.Num(); i++)
	{
		if (BorderColors[i] != FLinearColor(1, 1, 1))
		{
			BorderColors[i] = FLinearColor(1, 1, 1);
			break;
		}
	}

	BorderColors[BorderId] = FLinearColor(1, 0, 0, 0.85);
	selectedId = firstLineId + BorderId;
}

void USaveListWidgetClass::ScrollUp()
{
	if (firstLineId == 0) return;
	firstLineId--;

	for (int i = firstLineId, j = 0; i < firstLineId + 10; i++, j++)
	{
		SaveSlotNames[j] = SaveNameList[i];
	}

	for (int i = 0; i < BorderColors.Num(); i++)
	{
		if (BorderColors[i] == FLinearColor(1, 0, 0, 0.85))
		{
			if (i == 9) return;
			BorderColors[i + 1] = FLinearColor(1, 0, 0, 0.85);
			BorderColors[i] = FLinearColor(1, 1, 1);
			break;
		}
	}
}

void USaveListWidgetClass::ScrollDown()
{
	if (firstLineId + 11 > SaveNameList.Num()) return;
	firstLineId++;

	for (int i = firstLineId, j = 0; i < firstLineId + 10; i++, j++)
	{
		SaveSlotNames[j] = SaveNameList[i];
	}

	for (int i = 0; i < BorderColors.Num(); i++)
	{
		if (BorderColors[i] == FLinearColor(1, 0, 0, 0.85))
		{
			if (i == 0) return;
			BorderColors[i - 1] = FLinearColor(1, 0, 0, 0.85);
			BorderColors[i] = FLinearColor(1, 1, 1);
			break;
		}
	}
}

void USaveListWidgetClass::UpButtonPressed(bool isDown)
{
	if (isDown)
	{
		isUpButtonPressed = true;
		isScrolledAtLeastOnce = false;
		scrollCounter = baseScrollCounter;
	}
	else
	{
		isUpButtonPressed = false;
		if (!isScrolledAtLeastOnce) ScrollUp();
	}
}

void USaveListWidgetClass::DownButtonPressed(bool isDown)
{
	if (isDown)
	{
		isDownButtonPressed = true;
		isScrolledAtLeastOnce = false;
		scrollCounter = baseScrollCounter;
	}
	else
	{
		isDownButtonPressed = false;
		if (!isScrolledAtLeastOnce) ScrollDown();
	}
}

ESlateVisibility USaveListWidgetClass::GetArrowsVisibility()
{
	return SaveNameList.Num() > 10 ? ESlateVisibility::Visible : ESlateVisibility::Hidden;
}


void USaveListWidgetClass::DoubleClickToSave()
{
	OnSaveLineDoubleClick.Broadcast();
}
