// Fill out your copyright notice in the Description page of Project Settings.

#include "CPPProject.h"
#include "SaveLoadGameWidgetClass.h"

#define LOCTEXT_NAMESPACE "CPPProject"

#undef LOCTEXT_NAMESPACE

void USaveLoadGameWidgetClass::NativeConstruct()
{
	Super::NativeConstruct();
}

void USaveLoadGameWidgetClass::CancelButtonClick()
{
	OnWidgetClosed.Broadcast();
	this->RemoveFromParent();
}

FText USaveLoadGameWidgetClass::GetPlayerName()
{
	UMainGameInstance* instance = Cast<UMainGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
	return FText::FromString(instance->PlayerDefinition->Name);
}

void USaveLoadGameWidgetClass::CreateSaveSlot()
{
	if (!ButtonText.EqualTo(FText::FromString("Save")))
	{
		LoadGame();
	}
	else
	{

		UMainGameInstance* instance = Cast<UMainGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
		if (!SavesList->SaveNameList[SavesList->selectedId].EqualTo(FText::FromString("New Save")))
		{
			MessageBoxWidget = CreateWidget<UMessageBoxWidgetClass>(instance, MessageBoxClass);
			MessageBoxWidget->SetText(FText::FromString("Do you really want to override this save?"), FText::FromString("Yes"), FText::FromString("No"));
			MessageBoxWidget->AddToViewport();
			MessageBoxWidget->OnMessageBoxClosed.AddDynamic(this, &USaveLoadGameWidgetClass::SaveGame);
			return;
		}
		InputMessageBox = CreateWidget<UInputMessageBoxWidgetClass>(instance, InputMessageBoxClass);
		InputMessageBox->AddToViewport();
		InputMessageBox->SetKeyboardFocus();
		InputMessageBox->OnInputMessageBoxWidgetClosed.AddDynamic(this, &USaveLoadGameWidgetClass::SaveGame);
	}
}

void USaveLoadGameWidgetClass::SetWindowStatus(bool isSavingWindow)
{
	NativeConstruct();
	if (isSavingWindow)
	{
		WindowTitle = FText::FromString("Save Game");
		ButtonText = FText::FromString("Save");
		SavesList->SaveNameList.Insert(FText::FromString("New Save"), 0);
		SavesList->OnSaveLineDoubleClick.AddDynamic(this, &USaveLoadGameWidgetClass::CreateSaveSlot);
	}
	else
	{
		WindowTitle = FText::FromString("Load Game");
		ButtonText = FText::FromString("Load");
	}
}

void USaveLoadGameWidgetClass::SaveGame()
{
	if ((MessageBoxWidget && MessageBoxWidget->leftButtonPressed) || (InputMessageBox && InputMessageBox->dialogResult))
	{
		SaveManagerClass* saveManager = new SaveManagerClass();
		saveManager->CheckFolderExistence();
		if(InputMessageBox && InputMessageBox->dialogResult)
			saveManager->SaveGame(InputMessageBox->SaveName, GetWorld());
		else 
			saveManager->SaveGame(SavesList->SaveNameList[SavesList->selectedId], GetWorld());
	}

}

void USaveLoadGameWidgetClass::LoadGame()
{
	SaveManagerClass* saveManager = new SaveManagerClass();
	saveManager->CheckFolderExistence();
	saveManager->LoadGame(SavesList->SaveNameList[SavesList->selectedId], GetWorld());
}