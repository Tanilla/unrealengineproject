// Fill out your copyright notice in the Description page of Project Settings.

#include "CPPProject.h"
#include "EnemyBaseClass.h"
#include "EnemyHealthWidgetClass.h"


FText UEnemyHealthWidgetClass::GetEnemyName()
{
	return enemy->EnemyName;
}

float UEnemyHealthWidgetClass::GetEnemyHealthProcent()
{
	return (float)enemy->Health.CurrentHealth / (float)enemy->Health.MaxHealth;
}

