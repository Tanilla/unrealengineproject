// Fill out your copyright notice in the Description page of Project Settings.

#include "CPPProject.h"
#include "SaveManagerClass.h"

bool SaveManagerClass::CheckFolderExistence()
{	
	//check for 'saves' folder in user documents dir
	FString directory = FPlatformProcess::UserDir();
	FString fullpath = directory;
	fullpath.Append("\\OurProject");
	bool result = FPlatformFileManager::Get().GetPlatformFile().CreateDirectory(*fullpath);	
	if (result)
	{
		fullpath.Append("\\Saves");
		result = FPlatformFileManager::Get().GetPlatformFile().CreateDirectory(*fullpath);
	}
	return result;
}

bool SaveManagerClass::CheckSaveNameIsOkay()
{
	return false;
}

void SaveManagerClass::GatherAllData(FArchive& archive, UWorld* world, bool isSave)
{
	//TODO: Rename this method.

	//Saves all collected data to binary file

	USaveGameObject* saveObject = NewObject<USaveGameObject>(USaveGameObject::StaticClass());
	saveObject->SetWorld(world);
	if(isSave)
		saveObject->CollectAllData();
	archive << saveObject->PlayerLocation;
	archive << saveObject->PlayerRotation;
	archive << saveObject->CameraRotation;
	archive << saveObject->PlayerHealth;
	archive << saveObject->PlayerLevel;
	archive << saveObject->PlayerCurrentExperience;
	archive << saveObject->PlayerRequiredExperience;
	if (!isSave)
		saveObject->LoadAllData();
}

void SaveManagerClass::SaveGame(FText saveName, UWorld* world)
{
	
	FString directory = FPlatformProcess::UserDir();
	directory.Append("\\OurProject\\Saves\\");
	directory.Append(saveName.ToString());
	directory.Append(".txt");
	
	
	GatherAllData(binaryArray,world, true);

	if (binaryArray.Num() <= 0) return;
	
	FFileHelper::SaveArrayToFile(binaryArray, *directory);
	binaryArray.FlushCache();
	binaryArray.Empty();
}

void SaveManagerClass::LoadGame(FText saveName, UWorld* world)
{
	FString directory = FPlatformProcess::UserDir();
	directory.Append("\\OurProject\\Saves\\");
	directory.Append(saveName.ToString());
	directory.Append(".txt");

	if (!FFileHelper::LoadFileToArray(binaryArray, *directory))
	{
		return;
	}
	FMemoryReader load = FMemoryReader(binaryArray, true);
	load.Seek(0);
	GatherAllData(load, world, false);
	binaryArray.Empty();
	binaryArray.Close();
}
