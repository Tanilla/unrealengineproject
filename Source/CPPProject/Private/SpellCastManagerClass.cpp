// Fill out your copyright notice in the Description page of Project Settings.

#include "CPPProject.h"
#include "MainGameInstance.h"
#include "CPPProjectCharacter.h"
#include "GameHUD.h"
#include "EnemyBaseClass.h"
#include "SpellCastManagerClass.h"


USpellCastManagerClass::USpellCastManagerClass(const FObjectInitializer &ObjectInitializer)
: Super(ObjectInitializer)
{
	ConstructorHelpers::FObjectFinder<UDataTable>SpellDT(TEXT("DataTable'/Game/ThirdPerson/Data/SpellsDataTable.SpellsDataTable'"));
	if (SpellDT.Object)
		SpellsDataTable = SpellDT.Object;

	TickDelegate = FTickerDelegate::CreateUObject(this, &USpellCastManagerClass::Tick);
	TickHandle = FTicker::GetCoreTicker().AddTicker(TickDelegate);
}

bool USpellCastManagerClass::Tick(float DeltaSeconds)
{
	if (!isCasting) return true;

	if (!instance->PlayerTarget || GetDistance() > CurrentSpell.SpellRange + 5 || !CheckTrace())
	{
		StopCasting();
		return true;
	}

	CastTime += DeltaSeconds;

	if (CastTime >= CurrentSpell.CastTime)
	{
		if (CurrentSpell.SpellName == "Frostbolt")
		{
			FRotator rotation = FRotationMatrix::MakeFromX(GetWorld()->GetFirstPlayerController()->GetPawn()->GetActorLocation() - instance->PlayerTarget->GetActorLocation()).Rotator();
			FVector location = GetWorld()->GetFirstPlayerController()->GetPawn()->GetActorLocation();
			GetWorld()->SpawnActor<AActor>(CurrentSpell.SpellClass, location, rotation, FActorSpawnParameters());
			StopCasting();
		}
	}
	return true;
}


void USpellCastManagerClass::StartCasting(FName SpellName)
{
	if (isCasting) return;

	FSpellStruct spell = *SpellsDataTable->FindRow<FSpellStruct>(SpellName, "");

	if ((spell.SpellType == ESpellTypes::ST_Enemy || spell.SpellType == ESpellTypes::ST_Friendly) && !instance->PlayerTarget)
	{
		AGameHUD* hud = Cast<AGameHUD>(GetWorld()->GetFirstPlayerController()->GetHUD());
		hud->ErrorMessagesWidget->AddNewMessage(FText::FromString("No target"));
		return;
	}


	if ((spell.SpellType == ESpellTypes::ST_Enemy || spell.SpellType == ESpellTypes::ST_Friendly) && !CheckTrace())
	{
		AGameHUD* hud = Cast<AGameHUD>(GetWorld()->GetFirstPlayerController()->GetHUD());
		hud->ErrorMessagesWidget->AddNewMessage(FText::FromString("Can't see the target"));
		return;
	}

	float dist = GetDistance();
	if (GetDistance() > spell.SpellRange)
	{
		AGameHUD* hud = Cast<AGameHUD>(GetWorld()->GetFirstPlayerController()->GetHUD());
		hud->ErrorMessagesWidget->AddNewMessage(FText::FromString("Too far away"));
		return;
	}

	isCasting = true;
	CurrentSpell = spell;
	CastTime = 0;


}

bool USpellCastManagerClass::CheckTrace()
{
	FCollisionQueryParams TraceParams = FCollisionQueryParams(FName(TEXT("Trace")), true);
	TraceParams.bTraceComplex = true;
	TraceParams.bTraceAsyncScene = true;
	TraceParams.bReturnPhysicalMaterial = false;

	TArray<FHitResult> TraceHits;

	FCollisionObjectQueryParams ObjParams = FCollisionObjectQueryParams();
	ObjParams.AddObjectTypesToQuery(ECC_Pawn);
	ObjParams.AddObjectTypesToQuery(ECC_WorldStatic);

	FVector StartVector = GetWorld()->GetFirstPlayerController()->GetPawn()->GetActorLocation();
	FVector EndVector = instance->PlayerTarget->GetActorLocation();


	GetWorld()->LineTraceMultiByObjectType(TraceHits, StartVector, EndVector, ObjParams, TraceParams);

	bool result = true;

	for (int i = 0; i < TraceHits.Num(); i++)
	{
		if (TraceHits[i].Actor == instance->PlayerTarget)
			return result;
		if (TraceHits[i].Actor->IsA(AEnemyBaseClass::StaticClass()))
			continue;
		if (TraceHits[i].Actor->IsA(ACPPProjectCharacter::StaticClass()))
			continue;

		result = false;
	}
	return result;
}

float USpellCastManagerClass::GetDistance()
{
	return GetWorld()->GetFirstPlayerController()->GetPawn()->GetDistanceTo(instance->PlayerTarget);
}

void USpellCastManagerClass::StopCasting()
{
	isCasting = false;
	CastTime = -1;
}

UWorld* USpellCastManagerClass::GetWorld() const
{
	return instance->GetWorld();
}