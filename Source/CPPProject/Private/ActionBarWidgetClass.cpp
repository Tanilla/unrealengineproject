// Fill out your copyright notice in the Description page of Project Settings.

#include "CPPProject.h"
#include "ActionBarWidgetClass.h"

/*
For the future content. This method will rule ShadowOverlay on ability buttons. 
*/

void UActionBarWidgetClass::KeyPressed(FKey key)
{
	if (!instance) GetGameInstance();

	if (key == EKeys::Two)
	{
		if (!instance->PlayerTarget)
			GEngine->AddOnScreenDebugMessage(-1, 10, FColor::Red, "You have no target");
	}
}

FName UActionBarWidgetClass::GetSpellName()
{
	if (!instance) GetGameInstance();
	return instance->SpellCastingManager->CurrentSpell.SpellName;
}

ESlateVisibility UActionBarWidgetClass::IsCastbarVisible()
{
	if (!instance) GetGameInstance();
	return instance->SpellCastingManager->isCasting ? ESlateVisibility::Visible : ESlateVisibility::Hidden;
}

float UActionBarWidgetClass::CastTimeCompleted()
{
	if (!instance) GetGameInstance();
	return instance->SpellCastingManager->CastTime / instance->SpellCastingManager->CurrentSpell.CastTime;
}

FText UActionBarWidgetClass::GetCastTime()
{
	if (!instance) GetGameInstance();
	FNumberFormattingOptions numberFormat;
	numberFormat.MinimumFractionalDigits = 1;
	numberFormat.MaximumFractionalDigits = 1;
	return FText::Format(NSLOCTEXT("CPPProject", "CastTime", "{0} / {1}"), FText::AsNumber(instance->SpellCastingManager->CastTime, &numberFormat), FText::AsNumber(instance->SpellCastingManager->CurrentSpell.CastTime, &numberFormat));
}

void UActionBarWidgetClass::GetGameInstance()
{
	instance = Cast<UMainGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
}