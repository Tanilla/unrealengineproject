// Fill out your copyright notice in the Description page of Project Settings.

#include "CPPProject.h"
#include "ExperienceProgressBarWidgetClass.h"


float UExperienceProgressBarWidgetClass::GetExperiencePercent()
{
	if (!instance) instance = Cast<UMainGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));

	//convert to float if you want to have accurate number
	return (float)instance->PlayerLevel->CurrentExperience / (float)instance->PlayerLevel->RequiredExperience;
}

FText UExperienceProgressBarWidgetClass::GetExperienceText()
{
	if (!instance) instance = Cast<UMainGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));

	//just to be sure that we'll have no long numbers here

	FNumberFormattingOptions numberFormat;
	numberFormat.MinimumFractionalDigits = 0;
	numberFormat.MaximumFractionalDigits = 0;

	return FText::Format(NSLOCTEXT("CPPProject", "ProgressBarText", "{0}/{1}"), FText::AsNumber(instance->PlayerLevel->CurrentExperience, &numberFormat), FText::AsNumber(instance->PlayerLevel->RequiredExperience, &numberFormat));

}


