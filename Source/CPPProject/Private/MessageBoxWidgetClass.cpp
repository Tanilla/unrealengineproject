// Fill out your copyright notice in the Description page of Project Settings.

#include "CPPProject.h"
#include "MessageBoxWidgetClass.h"

void UMessageBoxWidgetClass::SetText(FText message, FText leftButton, FText rightButton)
{
	MessageText = message;
	LeftButtonText = leftButton;
	RightButtonText = rightButton;
}

void UMessageBoxWidgetClass::ButtonLeftPressed()
{
	leftButtonPressed = true;
	OnMessageBoxClosed.Broadcast();
	this->RemoveFromParent();
}

void UMessageBoxWidgetClass::ButtonRightPressed()
{
	leftButtonPressed = false;
	OnMessageBoxClosed.Broadcast();
	this->RemoveFromParent();
}


