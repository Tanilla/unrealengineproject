// Fill out your copyright notice in the Description page of Project Settings.

#include "CPPProject.h"
#include "LootItemWidgetClass.h"

UTexture2D* ULootItemWidgetClass::GetItemIcon()
{
	if (!((TAssetPtr<UTexture2D>)lootItem.ItemIcon).IsValid())
	{
		((TAssetPtr<UTexture2D>)lootItem.ItemIcon).LoadSynchronous();
	}
	return ((TAssetPtr<UTexture2D>)lootItem.ItemIcon).Get();
}


FName ULootItemWidgetClass::GetItemName()
{
	return lootItem.ItemName;
}


int32 ULootItemWidgetClass::GetAmountOfItem()
{
	return 1;
}


void ULootItemWidgetClass::SetLootItem(FLootItemStruct item)
{
	lootItem = item;
	LoadAssets();
}

void ULootItemWidgetClass::LoadAssets()
{/*
	if (((TAssetPtr<UTexture2D>)lootItem.ItemIcon).IsValid()) return;
	FStringAssetReference AssetToLoad;
	AssetToLoad = ((TAssetPtr<UTexture2D>)lootItem.ItemIcon).ToStringReference();

	FStreamableManager AssetLoader;
	AssetLoader.SimpleAsyncLoad(AssetToLoad);*/
}