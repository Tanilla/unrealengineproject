// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#ifndef __CPPPROJECT_H__
#define __CPPPROJECT_H__

#include "EngineMinimal.h"
#include "Runtime/UMG/Public/UMG.h"
#include "Runtime/UMG/Public/UMGStyle.h"
#include "Runtime/UMG/Public/IUMGModule.h"
#include "Runtime/UMG/Public/Blueprint/UserWidget.h"


#define COLLISION_SPELLS	ECC_GameTraceChannel1
#define COLLISION_WEAPON	ECC_GameTraceChannel2

#endif
