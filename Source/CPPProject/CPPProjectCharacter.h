// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.
#pragma once

#include "GameFramework/Character.h"
#include "MainGameInstance.h"
#include "EnemyBaseClass.h"
#include "FrostboltSpellProjectile.h"
#include "GameHUD.h"
#include "CPPProjectCharacter.generated.h"

UCLASS(config=Game)
class ACPPProjectCharacter : public ACharacter
{
	GENERATED_BODY()

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;
	
	UMainGameInstance* Instance;
	class AGameHUD* GameHUD;
	bool IsPaused;

	UFUNCTION(Category = "Character", meta = (Tooltip = "Sends pressed key to HUD"), BlueprintCallable)
		void CheckIfCastingBeforeJump();

	UFUNCTION(Category = "Character", meta = (Tooltip = "Sends pressed key to HUD"), BlueprintCallable)
		void CheckIfCastingBeforeStopJumping();

public:
	ACPPProjectCharacter();

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
		float BaseTurnRate;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = PlayerController)
		TArray<AEnemyBaseClass*> ListOfEnemiesInRange;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		bool RightButtonPressed;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
		float BaseLookUpRate;

	UFUNCTION()
		void ToggleMainMenu();

	virtual void Tick(float DeltaSeconds) override;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Camera)
		TSubclassOf<class AFrostboltSpellProjectile> FrostboltClass;

	UFUNCTION(Category = "Character", meta = (Tooltip = "Sends pressed key to HUD"), BlueprintCallable)
		void SendPressedKeyToHUD(FKey key);

	UFUNCTION(Category = "Character", meta = (Tooltip = "Creates a projectile ('Actor-Actor type') and launchs it"), BlueprintCallable)
		void CreateActorToActorProjectile();

	UFUNCTION(Category = "Character", meta = (Tooltip = "Sets RightMouseButton as Pressed"), BlueprintCallable)
		void RightMouseButtonPressed();

	UFUNCTION(Category = "Character", meta = (Tooltip = "Sets RightMouseButton as Released"), BlueprintCallable)
		void RightMouseButtonReleased();

protected:

	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

	/** 
	 * Called via input to turn at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/* Camera Inputs*/
	void CameraZoomIn();
	void CameraZoomOut();	
	void Turn(float Rate);
	void LookUp(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;
	// End of APawn interface
	virtual float TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;

	void LookForEnemies();
	bool CheckTrace(AActor* endPoint);
	void SelectTarget();
public:
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }
};

