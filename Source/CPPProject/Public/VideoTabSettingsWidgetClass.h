// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "TimerMessageBoxWidgetClass.h"
#include "VideoTabSettingsWidgetClass.generated.h"

/**
 * That class is so ugly! But i don't want to use all 'defualt' settings, so here we are.
 */
UENUM(BlueprintType)
enum class EVideoSettings : uint8
{
	VS_Low		UMETA(DisplayName = "Low"),
	VS_Medium	UMETA(DisplayName = "Medium"),
	VS_High		UMETA(DisplayName = "High"),
	VS_VeryHigh UMETA(DisplayName = "Very High")
};

UENUM(BlueprintType)
enum class EVideoResolution : uint8
{
	VR_1024_768		UMETA(DisplayName = "1024x768"),
	VR_1280_800		UMETA(DisplayName = "1280x800"),
	VR_1280_1024	UMETA(DisplayName = "1280x1024"),
	VR_1366_768		UMETA(DisplayName = "1366x768"),
	VR_1440_900		UMETA(DisplayName = "1440x900"),
	VR_1600_900		UMETA(DisplayName = "1600x900"),
	VR_1680_1050	UMETA(DisplayName = "1680x1050"),
	VR_1920_1080	UMETA(DisplayName = "1920x1080"),
	VR_2560_1440	UMETA(DisplayName = "2560x1440")
};

UENUM(BlueprintType)
enum class ESettingType : uint8
{
	ST_Resolution		UMETA(DisplayName = "Resolution Setting"),
	ST_Texture_Quality	UMETA(DisplayName = "Texture Quality Setting"),
	ST_Shadow_Quality	UMETA(DisplayName = "Shadow Quality Setting"),
	ST_Effects_Quality	UMETA(DisplayName = "Effects Quality Setting"),
	ST_Window_Mode		UMETA(DisplayName = "Window Mode Setting"),
	ST_View_Distance	UMETA(DisplayName = "View Distance Setting")
};

UENUM(BlueprintType)
enum class EWindowModeCustom : uint8
{
	WMC_Fullscreen		UMETA(DisplayName = "Fullscreen"),
	WMC_Windowed		UMETA(DisplayName = "Windowed"),
	WMC_Borderless		UMETA(DisplayName = "Borderless")
};

UCLASS()
class CPPPROJECT_API UVideoTabSettingsWidgetClass : public UUserWidget
{
	GENERATED_BODY()

	int8 resolutionsAmount;
	int8 settingsAmount;
	int8 windowAmount;


public:
	UFUNCTION(Category = "UIWidget", meta = (Tooltip = "Initialize SettingsWindow"), BlueprintCallable)
		void BeginPlay();

	UFUNCTION(Category = "UIWidget", meta = (Tooltip = "Initialize AmountsSettingsWindow"), BlueprintCallable)
		void SetAmounts();

	UFUNCTION(Category = "UIWidget", meta = (Tooltip = "Get next setting position"), BlueprintCallable)
		void GetNext(ESettingType blockName);

	UFUNCTION(Category = "UIWidget", meta = (Tooltip = "Get previous setting position"), BlueprintCallable)
		void GetPrevious(ESettingType blockName);

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = HUD)
		EVideoResolution currentResolution = EVideoResolution::VR_1920_1080;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = HUD)
		EVideoSettings currentTextureQuality = EVideoSettings::VS_High;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = HUD)
		EVideoSettings currentShadowQuality = EVideoSettings::VS_High;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = HUD)
		EVideoSettings currentEffectQuality = EVideoSettings::VS_High;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = HUD)
		EVideoSettings currentViewDistance = EVideoSettings::VS_High;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = HUD)
		EWindowModeCustom currentWindowMode = EWindowModeCustom::WMC_Fullscreen;

	UFUNCTION(Category = "UIWidget", meta = (Tooltip = "Get Resolution Text"), BlueprintCallable)
		FString GetResolutionText(uint8 index);

	UFUNCTION(Category = "UIWidget", meta = (Tooltip = "Get Quality Text"), BlueprintCallable)
		FString GetQualityText(uint8 index);

	UFUNCTION(Category = "UIWidget", meta = (Tooltip = "Get Window Mode Text"), BlueprintCallable)
		FString GetWindowModeText(uint8 index);
};
