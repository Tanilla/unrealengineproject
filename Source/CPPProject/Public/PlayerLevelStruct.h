// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/UserDefinedStruct.h"
#include "PlayerLevelStruct.generated.h"

/**
 * 
 */
UCLASS()
class CPPPROJECT_API UPlayerLevelStruct : public UUserDefinedStruct
{
public:
	GENERATED_UCLASS_BODY()

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = PlayerCharater)
		int32 Level;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = PlayerCharater)
		int32 CurrentExperience;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = PlayerCharater)
		int32 RequiredExperience;
	
};
