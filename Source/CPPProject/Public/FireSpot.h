// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "FHealthResourceStruct.h"
#include "MainGameInstance.h"
#include "FireSpot.generated.h"

UCLASS()
class CPPPROJECT_API AFireSpot : public AActor
{
	GENERATED_BODY()
	
public:	

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = Effects)
		UBoxComponent* BaseCollisionComponent;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Effects)
		UParticleSystem* FireWallPS;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Effects)
		UParticleSystemComponent* FireWallEffect;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = Effects)
		UMainGameInstance* instance;

	FTimerHandle timerHandle;

	AFireSpot();

	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;


	UFUNCTION()
		void OnOverlapBegin(class AActor* otherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
		void OnOverlapEnd(class AActor* otherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	void TimerTick();
	
};
