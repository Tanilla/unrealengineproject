// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/GameInstance.h"
#include "PlayerCharacterDefinitionStruct.h"
#include "SpellCastManagerClass.h"
#include "LootGenerationClass.h"
#include "PlayerCharacterStatsStruct.h"
#include "FHealthResourceStruct.h"
#include "PlayerLevelStruct.h"
#include "MainGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class CPPPROJECT_API UMainGameInstance : public UGameInstance
{
	GENERATED_UCLASS_BODY()
	
public:

	//stores player battle stats, perks etc
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = GameSettings)
		UPlayerCharacterStatsStruct* PlayerStats;

	//stores player name, gender, class etc
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = GameSettings)
		UPlayerCharacterDefinitionStruct* PlayerDefinition;

	//stores player level, experience etc
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = GameSettings)
		UPlayerLevelStruct* PlayerLevel;
	
	//stores player health amount, resource amount etc
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = GameSettings)
		FHealthResourceStruct PlayerHealth;

	//current selected enemy. Most spell will be send to him
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = GameSettings)
		class AEnemyBaseClass* PlayerTarget; 

	//particle which will be played after LevelUp
		UParticleSystem* LevelUpPatricle;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = GameSettings)
		USpellCastManagerClass* SpellCastingManager;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = GameSettings)
		ULootGenerationClass* LootGenerationManager;

	//adds gained experience
	void GainExperience(int ExperienceAmount, bool IsQuestReward);	
	
};
