// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "MainGameInstance.h"
#include "ExperienceProgressBarWidgetClass.generated.h"

/**
 * 
 */
UCLASS()
class CPPPROJECT_API UExperienceProgressBarWidgetClass : public UUserWidget
{
	GENERATED_BODY()
	UMainGameInstance* instance;

public:
	
	UPROPERTY(BlueprintReadWrite, Category = "UIWidget")
		ESlateVisibility OverlayVisibility = ESlateVisibility::Hidden; 

	//calculates percent of current experience
	UFUNCTION(Category = "UIWidget", meta = (Tooltip = "Returns procent of experience"), BlueprintCallable)
		float GetExperiencePercent();
	
	//returns text in CurrentExp / RequiredExp format
	UFUNCTION(Category = "UIWidget", meta = (Tooltip = "Returns text with current and required experience"), BlueprintCallable)
		FText GetExperienceText();	
};
