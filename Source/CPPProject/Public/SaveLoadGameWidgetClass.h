// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "UMG.h"
#include "InputMessageBoxWidgetClass.h"
#include "SaveManagerClass.h"
#include "MessageBoxWidgetClass.h"
#include "SaveListWidgetClass.h"
#include "MainGameInstance.h"
#include "SaveLoadGameWidgetClass.generated.h"

/**
 * 
 */
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FSaveLoadWidgetClosed);

UCLASS()
class CPPPROJECT_API USaveLoadGameWidgetClass : public UUserWidget
{
	GENERATED_BODY()

	

public:	

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "UIWidget")
		FText WindowTitle;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "UIWidget")
		FText ButtonText;

	UPROPERTY(BlueprintReadWrite, Category = "UIWidget")
		USaveListWidgetClass* SavesList;

	virtual void NativeConstruct() override;

	void SetWindowStatus(bool isSavingWindow);

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = HUD)
		TSubclassOf<class UInputMessageBoxWidgetClass> InputMessageBoxClass;

	class UInputMessageBoxWidgetClass* InputMessageBox;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = HUD)
		TSubclassOf<class UMessageBoxWidgetClass> MessageBoxClass;

	class UMessageBoxWidgetClass* MessageBoxWidget;

	UFUNCTION(Category = "UIWidget", meta = (Tooltip = "Closes current widget"), BlueprintCallable)
		void CancelButtonClick();

	UFUNCTION(Category = "UIWidget", meta = (Tooltip = "Returns player character name"), BlueprintCallable)
		FText GetPlayerName();

	UFUNCTION(Category = "UIWidget", meta = (Tooltip = "Opens Message Box for input save name"), BlueprintCallable)
		void CreateSaveSlot();
	
	UFUNCTION(Category = "UIWidget", meta = (Tooltip = "Saves game"), BlueprintCallable)
		void SaveGame();

	UFUNCTION(Category = "UIWidget", meta = (Tooltip = "Loads game"), BlueprintCallable)
		void LoadGame();

	UPROPERTY(BlueprintAssignable, Category = "UIWidget")
		FSaveLoadWidgetClosed OnWidgetClosed;
};
