// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "EnemyHealthWidgetClass.generated.h"

/**
 * 
 */
UCLASS()
class CPPPROJECT_API UEnemyHealthWidgetClass : public UUserWidget
{
	GENERATED_BODY()


public:
	class AEnemyBaseClass* enemy;

	UFUNCTION(Category = "EnemyClass", meta = (Tooltip = "Returns enemy name"), BlueprintCallable)
		FText GetEnemyName();

	UFUNCTION(Category = "EnemyClass", meta = (Tooltip = "Returns enemy health procent"), BlueprintCallable)
		float GetEnemyHealthProcent();
};
