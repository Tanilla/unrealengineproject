// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "FLootItemStruct.h"
#include "LootItemWidgetClass.generated.h"

/**
 * 
 */
UCLASS()
class CPPPROJECT_API ULootItemWidgetClass : public UUserWidget
{
	GENERATED_BODY()

	UPROPERTY()
		FLootItemStruct lootItem;

	void LoadAssets();

protected:
		
	UFUNCTION(Category = "Loot", meta = (Tooltip = "Returns item's Icon path"), BlueprintCallable)
		UTexture2D* GetItemIcon();

	UFUNCTION(Category = "Loot", meta = (Tooltip = "Returns item's name"), BlueprintCallable)
		FName GetItemName();

	UFUNCTION(Category = "Loot", meta = (Tooltip = "Returns amount of item dropped"), BlueprintCallable)
		int32 GetAmountOfItem();


public:
	UFUNCTION(Category = "Loot", meta = (Tooltip = "Set associated Loot Item"), BlueprintCallable)
		void SetLootItem(FLootItemStruct item);
};
