// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "FSpellStruct.generated.h"

UENUM(BlueprintType)
enum class ESpellTypes : uint8
{
	ST_Self,
	ST_Friendly,
	ST_Enemy,
	ST_AoE,
	ST_AutoCastAoE
};


UENUM(BlueprintType)
enum class EDamageTypes : uint8
{
	DT_Fire, 
	DT_Water, 
	DT_Eath, 
	DT_Mental, 
	DT_Physical
};

USTRUCT(BlueprintType)
struct CPPPROJECT_API FSpellStruct : public FTableRowBase
{
public:
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spells")
		int32 SpellId;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spells")
		FName SpellName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spells")
		int32 ResourceCost;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spells")
		ESpellTypes SpellType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spells")
		float SpellRange;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spells")
		float CastTime;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spells")
		float CooldownTime;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spells")
		float Duration;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spells")
		EDamageTypes DamageType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spells")
		float DamageAmount;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spells")
		FText Description;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spells")
		TAssetPtr<UTexture> SpellIcon;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spells")
		TAssetPtr<UParticleSystem> SpellEffect;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spells")
		TSubclassOf<class AActor> SpellClass;
};
