// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/World.h"
#include "MainGameInstance.h"
#include "CPPProjectCharacter.h"
#include "SaveGameObject.generated.h"

/**
 * This class collects data from all classes to save it or load
 */
UCLASS()
class CPPPROJECT_API USaveGameObject : public UObject
{
	GENERATED_BODY()

	UWorld* world;
	UMainGameInstance* instance;

public:
	UPROPERTY()
		FVector PlayerLocation;

	UPROPERTY()
		FRotator PlayerRotation;

	UPROPERTY()
		FRotator CameraRotation;

	UPROPERTY()
		int32 PlayerHealth;
	
	UPROPERTY()
		int32 PlayerLevel;

	UPROPERTY()
		int32 PlayerCurrentExperience;
	
	UPROPERTY()
		int32 PlayerRequiredExperience;

	UFUNCTION()
		void SetWorld(UWorld* currentWorld);

	UFUNCTION()
		void CollectAllData();

	UFUNCTION()
		void LoadAllData();

};
