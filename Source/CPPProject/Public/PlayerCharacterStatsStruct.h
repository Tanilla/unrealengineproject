// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/UserDefinedStruct.h"
#include "PlayerCharacterStatsStruct.generated.h"

/**
 * 
 */
UCLASS()
class CPPPROJECT_API UPlayerCharacterStatsStruct : public UUserDefinedStruct
{
public:
	GENERATED_UCLASS_BODY()

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = PlayerCharater)
	int32 Stamina;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = PlayerCharater)
		int32 Strength;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = PlayerCharater)
		int32 Intellect;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = PlayerCharater)
		int32 Agility;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = PlayerCharater)
		int32 Charisma;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = PlayerCharater)
		int32 Luck;	
};
