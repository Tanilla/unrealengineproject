// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "InputMessageBoxWidgetClass.generated.h"

/**
 * 
 */

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FInputMessageBoxWidgetClosed);

UCLASS()
class CPPPROJECT_API UInputMessageBoxWidgetClass : public UUserWidget
{
	GENERATED_BODY()
	
public:
	
	UPROPERTY(BlueprintReadWrite, Category = "UIWidget")
		FText SaveName;

	UPROPERTY(BlueprintAssignable, Category = "UIWidget")
		FInputMessageBoxWidgetClosed OnInputMessageBoxWidgetClosed;

	UFUNCTION(Category = "UIWidget", meta = (Tooltip = "Close this widget and store save name"), BlueprintCallable)
		void SaveGame();

	UFUNCTION(Category = "UIWidget", meta = (Tooltip = "Return without saving"), BlueprintCallable)
		void CancelSaving();

	bool dialogResult = false;	
};
