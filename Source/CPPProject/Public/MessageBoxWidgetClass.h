// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "MessageBoxWidgetClass.generated.h"

/**
 * 
 */


DECLARE_DYNAMIC_MULTICAST_DELEGATE(FMessageBoxWidgetClosed);

UCLASS()
class CPPPROJECT_API UMessageBoxWidgetClass : public UUserWidget
{
	GENERATED_BODY()
	
public:

	UPROPERTY(BlueprintReadOnly, Category = "UIWidget")
		FText MessageText;

	UPROPERTY(BlueprintReadOnly, Category = "UIWidget")
		FText LeftButtonText;

	UPROPERTY(BlueprintReadOnly, Category = "UIWidget")
		FText RightButtonText;

	UPROPERTY(BlueprintReadOnly, Category = "UIWidget")
		bool leftButtonPressed = false;

	UFUNCTION(Category = "UIWidget", meta = (Tooltip = "Set MessageBox text and buttons' names"), BlueprintCallable)
		void SetText(FText message, FText leftButton, FText rightButton);

	UFUNCTION(Category = "UIWidget", meta = (Tooltip = "Detect Right Button was pressed"), BlueprintCallable)
		void ButtonRightPressed();

	UFUNCTION(Category = "UIWidget", meta = (Tooltip = "Detect Left Button was pressed"), BlueprintCallable)
		void ButtonLeftPressed();

	UPROPERTY(BlueprintAssignable, Category = "UIWidget")
		FMessageBoxWidgetClosed OnMessageBoxClosed;
	
	
};
