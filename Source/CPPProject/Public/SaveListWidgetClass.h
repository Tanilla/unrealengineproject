// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "InputMessageBoxWidgetClass.h"
#include "SaveListWidgetClass.generated.h"

/**
 * 
 */

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FDoubleClickToSave);
UCLASS()
class CPPPROJECT_API USaveListWidgetClass : public UUserWidget
{
	GENERATED_BODY()

	const float baseScrollCounter = 0.3;

	uint16 firstLineId;
	bool isMouseInList;
	float scrollCounter;
	bool isScrolledAtLeastOnce = false;
	
public: 
	
	uint16 selectedId;

	UPROPERTY(BlueprintAssignable, Category = "UIWidget")
		FDoubleClickToSave OnSaveLineDoubleClick;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "UIWidget")
		TArray<FText> SaveSlotNames;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "UIWidget")
		TArray<FLinearColor> BorderColors;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "UIWidget")
		TArray<FText> SaveNameList;

	UPROPERTY(BlueprintReadWrite, Category = "UIWidget")
		UBorder* UpButtonBorder;

	UPROPERTY(BlueprintReadWrite, Category = "UIWidget")
		UBorder* DownButtonBorder;

	UPROPERTY(BlueprintReadWrite, Category = "UIWidget")
		bool isUpButtonPressed = false;

	UPROPERTY(BlueprintReadWrite, Category = "UIWidget")
		bool isDownButtonPressed = false;

	virtual void NativeConstruct() override;

	virtual void NativeTick(const FGeometry &MyGeometry, float InDeltaTime) override;

	virtual FReply NativeOnMouseWheel(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent) override;


	UFUNCTION(Category = "UIWidget", meta = (Tooltip = "Scrolls list of saves up"), BlueprintCallable)
		void ScrollUp();

	UFUNCTION(Category = "UIWidget", meta = (Tooltip = "Scrolls list of saves down"), BlueprintCallable)
		void ScrollDown();

	UFUNCTION(Category = "UIWidget", meta = (Tooltip = "Select save from list of saves"), BlueprintCallable)
		void SelectSaveRow(int32 BorderId);

	UFUNCTION(Category = "UIWidget", meta = (Tooltip = "Detect pressed \"Up Button\""), BlueprintCallable)
		void UpButtonPressed(bool isDown);

	UFUNCTION(Category = "UIWidget", meta = (Tooltip = "Detect pressed \"Down Button\""), BlueprintCallable)
		void DownButtonPressed(bool isDown);

	UFUNCTION(Category = "UIWidget", meta = (Tooltip = "Detect double Click on one of the save line to save the game under the same name"), BlueprintCallable)
		void DoubleClickToSave();

	UFUNCTION(Category = "UIWidget", meta = (Tooltip = "Returns arrows button visibility"), BlueprintCallable)
		ESlateVisibility GetArrowsVisibility();
};
