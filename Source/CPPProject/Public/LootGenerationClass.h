// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/UserDefinedStruct.h"
#include "FLootItemStruct.h"
#include "LootGenerationClass.generated.h"

/**
 * 
 */
UCLASS()
class CPPPROJECT_API ULootGenerationClass : public UObject
{
	GENERATED_UCLASS_BODY()

public:

	UFUNCTION(Category = "Loot", meta = (Tooltip = "Generates requested amount of loot"))
		TArray<FLootItemStruct> GenerateLootList(int32 maxNumberOfItems);

	UPROPERTY(BlueprintReadOnly, Category = "Loot")
		UDataTable* ItemsDataTable;
	
};
