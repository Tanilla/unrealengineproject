// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/HUD.h"
#include "MainGameInstance.h"
#include "HealthResourcePanel.h"
#include "ActionBarWidgetClass.h"
#include "ErrorMessagesWidgetClass.h"
#include "InGameMenuWidgetClass.h"
#include "GameHUD.generated.h"



UCLASS()

class CPPPROJECT_API AGameHUD : public AHUD
{
	GENERATED_UCLASS_BODY()
	
public:
	virtual void BeginPlay() override;

	//method which sends pressed key to ActonBarWidget. For future purpose 
	void SendKeyToActionBar(FKey key);

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "UIWidget")
		UMainGameInstance* Instance;
	

	//Player's health and resource panel
	UPROPERTY(BlueprintReadOnly, Category = "UIWidget")
	class UHealthResourcePanel* HealthResourcePanel;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "UIWidget")
		TSubclassOf<class UHealthResourcePanel> HealthResourcePanelClass;


	//In game menu with resume/save/load/exit buttons. Toggles by pressing 'Home' key.

	UPROPERTY(BlueprintReadOnly, Category = "UIWidget")
	class UInGameMenuWidgetClass* InGameMenu;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "UIWidget")
		TSubclassOf<class UInGameMenuWidgetClass> InGameMenuClass;


	//Player's ActionBar.

	UPROPERTY(BlueprintReadOnly, Category = "UIWidget")
	class UActionBarWidgetClass* ActionBar;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "UIWidget")
		TSubclassOf<class UActionBarWidgetClass> ActionBarClass;


	//Error messages widget.

	UPROPERTY(BlueprintReadOnly, Category = "UIWidget")
	class UErrorMessagesWidgetClass* ErrorMessagesWidget;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "UIWidget")
		TSubclassOf<class UErrorMessagesWidgetClass> ErrorMessagesClass;

	//Loot window widget.

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Enemy")
		class ULootWidgetClass* LootWidget;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Enemy")
		TSubclassOf<class ULootWidgetClass> LootWidgetClass;


	//Restore visibility of health panel and action bar after InGameMenu being closed.
	UFUNCTION(Category = "HUD", meta = (Tooltip = "SetGameMenusVisible"))
		void SetGameMenusVisibility();

	UFUNCTION(Category = "HUD", meta = (Tooltip = "SetLootItemsForLoorWidget"))
		void SetLootWindowActor(AActor* lootActor);

	virtual void DrawHUD() override;

	//flag for InGameMenu
	bool IsInGameMenu = false;	
};
