// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "SettingsWindowWidgetClass.h"
#include "SaveLoadGameWidgetClass.h"
#include "InGameMenuWidgetClass.generated.h"

/**
 * 
 */
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FMainMenuWidgetClosed);

UCLASS()
class CPPPROJECT_API UInGameMenuWidgetClass : public UUserWidget
{
	GENERATED_BODY()

	UMainGameInstance* instance;
	

public:
	//Variable names speak for themselves


	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = HUD)
		TSubclassOf<class USettingsWindowWidgetClass> SettingsWindowClass;

	class USettingsWindowWidgetClass* SettingsWindow;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = HUD)
		TSubclassOf<class USaveLoadGameWidgetClass> SaveLoadWindowClass;

	class USaveLoadGameWidgetClass* SaveLoadWindow;

	UFUNCTION(Category = "UIWidget", meta = (Tooltip = "Resume Game Button Click"), BlueprintCallable)
		void OnResumeGame_Click();
	
	UFUNCTION(Category = "UIWidget", meta = (Tooltip = "Settings Button Click"), BlueprintCallable)
		void OnSettings_Click();

	UFUNCTION(Category = "UIWidget", meta = (Tooltip = "Save Game Button Click"), BlueprintCallable)
		void OnSaveGame_Click();

	UFUNCTION(Category = "UIWidget", meta = (Tooltip = "Load Game Button Click"), BlueprintCallable)
		void OnLoadGame_Click();

	UFUNCTION(Category = "UIWidget", meta = (Tooltip = "Exit To Windows Button Click"), BlueprintCallable)
		void OnExitToWindows_Click();

	UFUNCTION(Category = "UIWidget", meta = (Tooltip = "Set In Game Menu Widget Visible"))
		void SetWidgetVisible();

	UPROPERTY(BlueprintAssignable, Category = "ClosedDelegate")
		FMainMenuWidgetClosed OnMainMenuWidgetClosed;
};
