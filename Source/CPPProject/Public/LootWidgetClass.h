// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "LootItemWidgetClass.h"
#include "LootWidgetClass.generated.h"

/**
 * 
 */
UCLASS()
class CPPPROJECT_API ULootWidgetClass : public UUserWidget
{
	GENERATED_BODY()

public:
	
	UPROPERTY()
		AActor* enemyObject;

	UPROPERTY(BlueprintReadOnly, Category = "Loot System")
		FText ContainerName;

	UFUNCTION(Category = "Loot System", meta = (Tooltip = "Creates and adds to the list loot item widgets"), BlueprintCallable)
		void CreateItemsList();

	UFUNCTION(Category = "Loot System", meta = (Tooltip = "Returns the flag indicating that scroll is visible or not"), BlueprintCallable)
		bool IsScrollNeeded();

	UFUNCTION()
		virtual void OnMouseLeave(const FPointerEvent& MouseEvent);

	UFUNCTION()
		virtual void Destruct();

	UFUNCTION(Category = "Loot System", meta = (Tooltip = "Clears list of ItemWidgets and adds all items to player inventory"), BlueprintCallable)
		void TakeAll();

	UPROPERTY(BlueprintReadWrite, Category = "Loot System")
		UGridPanel* gridPanel;

	//LootItemWidget

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Loot System")
		TSubclassOf<class ULootItemWidgetClass> LootItemWidgetClass;

	void SetEnemyObject(AActor* enemy);

	void ClearItems();
};
