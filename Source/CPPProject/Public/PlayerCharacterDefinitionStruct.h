// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/UserDefinedStruct.h"
#include "PlayerCharacterDefinitionStruct.generated.h"

/**
 * 
 */
UCLASS()
class CPPPROJECT_API UPlayerCharacterDefinitionStruct : public UUserDefinedStruct
{
public:
	GENERATED_UCLASS_BODY()
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = PlayerCharater)
		FString Name;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = PlayerCharater)
		int32 Class;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = PlayerCharater)
		bool Gender;	
};
