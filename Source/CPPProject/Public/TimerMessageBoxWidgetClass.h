// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "TimerMessageBoxWidgetClass.generated.h"

/**
 * 
 */

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FTimerMessageBoxWidgetClosed);

UCLASS()
class CPPPROJECT_API UTimerMessageBoxWidgetClass : public UUserWidget
{
	GENERATED_BODY()

	UGameUserSettings* settings;
	float currentCounter = 20;	

public:
	UFUNCTION(Category = "UIWidget", meta = (Tooltip = "Mouse click on \"Yes\" button"), BlueprintCallable)
		void YesButtonClick();
	
	UFUNCTION(Category = "UIWidget", meta = (Tooltip = "Mouse click on \"No\" button"), BlueprintCallable)
		void NoButtonClick();

	UFUNCTION(Category = "UIWidget", meta = (Tooltip = "Get current text for messageBox"), BlueprintCallable)
		FText GetMessageBoxText();

	UPROPERTY(BlueprintAssignable, Category = "UIWidget")
		FTimerMessageBoxWidgetClosed OnMessageBoxClosed;

	UFUNCTION(Category = "UIWidget", meta = (Tooltip = "Get current text for messageBox"), BlueprintCallable)
		void Timer(float delta);

	virtual void NativeConstruct() override;
	
	virtual void NativeTick(const FGeometry &MyGeometry, float InDeltaTime) override;
	
};
