// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "FrostboltSpellProjectile.generated.h"


//Simple Projectile class. I didn't find a solution to use timeline in code - so i have to did to in blueprints. 
UCLASS()
class CPPPROJECT_API AFrostboltSpellProjectile : public AActor
{
	GENERATED_BODY()
	

public:	
	AFrostboltSpellProjectile();

	virtual void BeginPlay() override;
	
	virtual void Tick( float DeltaSeconds ) override;

};
