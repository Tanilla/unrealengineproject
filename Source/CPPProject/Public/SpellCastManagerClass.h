// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Object.h"
#include "FSpellStruct.h"
#include "SpellCastManagerClass.generated.h"

/**
 * 
 */
UCLASS()
class CPPPROJECT_API USpellCastManagerClass : public UObject
{
	GENERATED_UCLASS_BODY()
	
	class UMainGameInstance* instance;

	FTickerDelegate TickDelegate;
	FDelegateHandle TickHandle;

public:

	UPROPERTY(BlueprintReadOnly, Category = "Spells")
		float CastTime = -1; 

	UPROPERTY(BlueprintReadOnly, Category = "Spells")
		bool isCasting;

	UPROPERTY(BlueprintReadOnly, Category = "Spells")
		FSpellStruct CurrentSpell;

	UPROPERTY(BlueprintReadOnly, Category = "Spells")
		UDataTable* SpellsDataTable;

	UFUNCTION(Category = "Spells", meta = (Tooltip = "Start spell casting"), BlueprintCallable)
		void StartCasting(FName SpellName);

	UFUNCTION(Category = "Spells", meta = (Tooltip = "Stop spell casting"), BlueprintCallable)
		void StopCasting();
	
	bool CheckTrace();
	float GetDistance();
	bool Tick(float DeltaSeconds);
	virtual UWorld* GetWorld() const override;
};
