// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "FLootItemStruct.generated.h"

UENUM(BlueprintType)
enum class ECharactersClass : uint8
{
	CC_None,
	CC_Warrior, 
	CC_Mage, 
	CC_Rogue
};

UENUM(BlueprintType)
enum class ECharacter : uint8
{
	C_None,
	C_Alchemist, 
	C_Bard, 
	C_Crook,
	C_FireMage, 
	C_Hunter,
	C_Priest,
	C_ProtWarrior,
	C_Summoner, 
	C_Tactic
};

UENUM(BlueprintType)
enum class EItemTypes : uint8
{
	IT_Craft,
	IT_Potion,
	IT_Money, 
	IT_Junk, 
	IT_QuestItem,
	IT_Rune, 
	IT_LoreBook,
	IT_Hat, 
	IT_Shoulders, 
	IT_Chest,
	IT_Pants, 
	IT_Boots, 
	IT_2HWeapon,
	IT_1HWeapon,
	IT_OffHand,
	IT_Jewelry,
	IT_Artefact
};

UENUM(BlueprintType)
enum class EItemQuality : uint8
{
	IQ_Common,
	IQ_Enchanted,
	IQ_Rare,
	IQ_Legendary, 
	IQ_Unique
};

USTRUCT(BlueprintType)
struct CPPPROJECT_API FLootItemStruct : public FTableRowBase
{

public:
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Items")
		int32 ItemId;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Items")
		TAssetPtr<UStaticMesh> ItemModel;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Items")
		TAssetPtr<UTexture2D> ItemIcon;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Items")
		FName ItemName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Items")
		EItemTypes ItemType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Items")
		ECharactersClass ClassRestriction;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Items")
		ECharacter CharacterRestiction;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Items")
		FText LoreComment;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Items")
		EItemQuality ItemQuality;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Items")
		int32 BaseItemPrice;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Items")
		FText ItemDescription;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Items")
		int32 MaxQuantity;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Items")
		int32 RollWeight;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Items")
		int32 ItemLevel;
};
