// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/PlayerController.h"
#include "CPPProjectPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class CPPPROJECT_API ACPPProjectPlayerController : public APlayerController
{
	GENERATED_BODY()

protected:
	UFUNCTION(Category = "PlayerController", meta = (Tooltip = "Event for Right Mouse Button being pressed"), BlueprintCallable)
		void RightMouseButtonPressed();
	
	UFUNCTION(Category = "PlayerController", meta = (Tooltip = "Event for Right Mouse Button being released"), BlueprintCallable)
		void RightMouseButtonReleased();

};
