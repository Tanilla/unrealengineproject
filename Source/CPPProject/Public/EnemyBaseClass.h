// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "MainGameInstance.h"
#include "FLootItemStruct.h"
#include "FHealthResourceStruct.h"
#include "EnemyHealthWidgetClass.h"
#include "LootWidgetClass.h"
#include "EnemyBaseClass.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE(FRightMouseButtonPressed);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FRightMouseButtonReleased);

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FEnemyDespawned);

UCLASS()
class CPPPROJECT_API AEnemyBaseClass : public ACharacter
{
	GENERATED_BODY()

	bool CheckIfSeeingPlayer(); 
	FTimerHandle handle;



public:
	// Sets default values for this character's properties
	AEnemyBaseClass();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	UFUNCTION(Category = "Enemy")
		void TimerHandle_LifeExpired();

	UPROPERTY(BlueprintReadOnly, Category = "Enemy")
		FHealthResourceStruct Health;

	UPROPERTY(BlueprintReadWrite, Category = "Enemy")
		FText EnemyName;

	UPROPERTY(BlueprintReadWrite, Category = "Enemy")
		bool IsAlive = true;

	//This widget will be shown above Enemy head after user come close enough (user must also see this enemy to see widget as well)
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Enemy")
		class UEnemyHealthWidgetClass* HealthWidget;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Enemy")
		TSubclassOf<class UEnemyHealthWidgetClass> HealthWidgetClass;

    //Changing by collision sphere events
	UPROPERTY(BlueprintReadWrite, Category = "Enemy")
		bool isPlayerInRange = false;
	
	//Changing by CPPProjectCharacter LookForEnemies method
	UPROPERTY(BlueprintReadWrite, Category = "Enemy")
		bool IsWatching = false;

	//Changing by collision box events
	UPROPERTY(BlueprintReadWrite, Category = "Enemy")
		bool isSelected = false;
	
	//Indicates to user if this enemy is selected or not
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Enemy")
		UStaticMeshComponent* TargetCircle;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Enemy")
		UBoxComponent* MouseCollisionBox;

	UPROPERTY(BlueprintReadOnly, Category = "Enemy")
		class UMainGameInstance* instance;

	UPROPERTY(BlueprintReadWrite, Category = "Enemy")
		FVector LastHitImpactVector;

	UPROPERTY(EditDefaultsOnly, Category = "Enemy")
		UParticleSystem* LootGlowParticle;

	UFUNCTION(Category = "Enemy", meta = (Tooltip = "Select this enemy as player target and deselect previous one"), BlueprintCallable)
		void SelectAsPlayerTarget();		

	UFUNCTION(Category = "Enemy", meta = (Tooltip = "Start glowing effect after ending of death animation"), BlueprintCallable)
		void StartGlowing();
	
	UPROPERTY(BlueprintAssignable, Category = "Enemy")
		FRightMouseButtonPressed RightMouseButtonPressed;

	UPROPERTY(BlueprintAssignable, Category = "Enemy")
		FRightMouseButtonReleased RightMouseButtonReleased;

	UPROPERTY(BlueprintAssignable, Category = "Enemy")
		FEnemyDespawned EnemyDespawned;

	UPROPERTY(BlueprintReadOnly, Category = "Enemy")
		TArray<FLootItemStruct> LootArray;

	UFUNCTION()
		void ShowLootWidget();

	virtual float TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;
};
