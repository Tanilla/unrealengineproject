// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "FileManager.h"
#include "MainGameInstance.h"
#include "SaveGameObject.h"

/**
 * 
 */
class CPPPROJECT_API SaveManagerClass
{
	FBufferArchive binaryArray;

public:
	bool CheckFolderExistence();
	bool CheckSaveNameIsOkay();
	void GatherAllData(FArchive& archive, UWorld* world, bool isSave);
	void SaveGame(FText saveName, UWorld* world);
	void LoadGame(FText saveName, UWorld* world);
};
