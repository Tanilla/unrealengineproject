// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "MainGameInstance.h"
#include "HealthResourcePanel.generated.h"

/**
 * 
 */
UCLASS()
class CPPPROJECT_API UHealthResourcePanel : public UUserWidget
{
	GENERATED_BODY()	
	
public:
	//calculates current player health percentage
	UFUNCTION(Category = "HealthResourcePanel", meta = (Tooltip = "GetHP"), BlueprintCallable)
		float HealthPercent(UMainGameInstance* instance);

	//calculates current player resource percentage
	UFUNCTION(Category = "HealthResourcePanel", meta = (Tooltip = "GetResource"), BlueprintCallable)
		float ResourcePercent(UMainGameInstance* instance);

	//returns player Hero name
	UFUNCTION(Category = "HealthResourcePanel", meta = (Tooltip = "GetName"), BlueprintCallable)
		FString GetHeroName(UMainGameInstance* instance);
};
