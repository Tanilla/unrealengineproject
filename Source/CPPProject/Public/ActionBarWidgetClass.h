// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "MainGameInstance.h"
#include "ActionBarWidgetClass.generated.h"

/**
 * 
 */
UCLASS()
class CPPPROJECT_API UActionBarWidgetClass : public UUserWidget
{
	GENERATED_BODY()

	UMainGameInstance* instance;
	void GetGameInstance();
public: 	
	UFUNCTION(Category = "UIWidget", meta = (Tooltip = "Returns status of ShadowOverlay"), BlueprintCallable)
		void KeyPressed(FKey key);

	UFUNCTION(Category = "UIWidget", meta = (Tooltip = "Returns percent of cast completed"), BlueprintCallable)
		float CastTimeCompleted();

	UFUNCTION(Category = "UIWidget", meta = (Tooltip = "Returns visibility of cast bar"), BlueprintCallable)
		ESlateVisibility IsCastbarVisible();

	UFUNCTION(Category = "UIWidget", meta = (Tooltip = "Returns spell name"), BlueprintCallable)
		FName GetSpellName();

	UFUNCTION(Category = "UIWidget", meta = (Tooltip = "Returns pair of cast time left and full cast time name"), BlueprintCallable)
		FText GetCastTime();
};
