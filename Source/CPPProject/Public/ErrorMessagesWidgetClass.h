// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "ErrorMessagesWidgetClass.generated.h"

/**
 * 
 */
UCLASS()
class CPPPROJECT_API UErrorMessagesWidgetClass : public UUserWidget
{
	GENERATED_BODY()
	

public:

	UPROPERTY(BlueprintReadOnly, Category = "UIWidget")
		TArray<FText> MessagesArray;

	UPROPERTY()
		TArray<FTimerHandle> Timers;

	UFUNCTION(BlueprintCallable, Category = "UIWidget")
		void AddNewMessage(FText newMessage);

	UFUNCTION(BlueprintCallable, Category = "UIWidget")
		void RemoveMessageOnTimer();	

	virtual void NativeConstruct() override;
};
