// Fill out your copyright notice in the Description page of Project Settings.
#pragma once

#include "FHealthResourceStruct.generated.h"
/**
 */
USTRUCT(BlueprintType)
struct CPPPROJECT_API FHealthResourceStruct
{
	GENERATED_BODY()

public:

	FHealthResourceStruct();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = PlayerCharater)
		int32 MaxHealth;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = PlayerCharater)
		int32 MaxResource;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = PlayerCharater)
		int32 CurrentHealth;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = PlayerCharater)
		int32 CurrentResource;

};
