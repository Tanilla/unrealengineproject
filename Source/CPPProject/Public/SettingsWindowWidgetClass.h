// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "VideoTabSettingsWidgetClass.h"
#include "MainGameInstance.h"
#include "SettingsWindowWidgetClass.generated.h"

/**
 * 
 */

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FSettingsWidgetClosed);

UCLASS()
class CPPPROJECT_API USettingsWindowWidgetClass : public UUserWidget
{
	GENERATED_BODY()

	UGameUserSettings* settings;

	void SetResolution();
	void SetWindowMode();
	void SetVideoSetting(EVideoSettings videoSetting, ESettingType type);
	

public:
	UFUNCTION(Category = "UIWidget", meta = (Tooltip = "CloseSettingsWindow"), BlueprintCallable)
		void OnCancel_Click();

	UFUNCTION(Category = "UIWidget", meta = (Tooltip = "AcceptSettings"), BlueprintCallable)
		void OnAccept_Click();

	UFUNCTION(Category = "UIWidget", meta = (Tooltip = "Remove Background Fading"))
		void RemoveFading();

	UPROPERTY(BlueprintAssignable, Category = "UIWidget")
		FSettingsWidgetClosed OnWidgetClosed;
	
	UPROPERTY(BlueprintReadWrite, Category = "UIWidget")
		UVideoTabSettingsWidgetClass* VideoSettingTab;


	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "UIWidget")
		UTimerMessageBoxWidgetClass* MessageBoxWidget;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "UIWidget")
		TSubclassOf<class UTimerMessageBoxWidgetClass> MessageBoxClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "UIWidget")
		UUserWidget* FadingWidget;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "UIWidget")
		TSubclassOf<class UUserWidget> FadingWidgetClass;
};
