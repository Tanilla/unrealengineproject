// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "CPPProject.h"
#include "CPPProjectGameMode.h"
#include "MainGameInstance.h"
#include "CPPProjectCharacter.h"

ACPPProjectGameMode::ACPPProjectGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPerson/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}

	static ConstructorHelpers::FClassFinder<AGameHUD>HUDobj(TEXT("/Game/ThirdPerson/Blueprints/GameHUDBlueprint"));
	if (HUDobj.Class != NULL)
		HUDClass = HUDobj.Class;

	static ConstructorHelpers::FClassFinder<APlayerController>PlayerControllerObj(TEXT("/Game/ThirdPerson/Blueprints/GamePlayerController"));
	if (PlayerControllerObj.Class != NULL)
		PlayerControllerClass = PlayerControllerObj.Class;
}
